// Include the amplifier library
#include <HX711_ADC.h>

// Define a reader
HX711_ADC LoadCell(4,5); // DT, SCK : pin number at which we connect. dout pin = 4, socket pin = 5;
long stabilizingtime = 2000; // Stabilization time of the amplifier [ms]
float inittime; // time after initialization
bool motorFired = 0;

/*
// moving average:
float leftdata;
float centraldata;
float rightdata;
*/

void setup() {
  
pinMode(10, OUTPUT); // Trigger of my motor from pin 10
digitalWrite(10, LOW); // OPEN THE RELAY AT THE BEGINNING OF THE LOOP
  
Serial.begin(9600); // Inits the communication with the PC with speed 9600 [bit/s]
Serial.println("Waiting for initialization...");

// Define a load cell:
LoadCell.begin(); // Starts the initialization
LoadCell.start(stabilizingtime); // Starts aquisizion after zeroing
LoadCell.setCalFactor(-925); // Setup calibration Factor to be modified after calibration (multiplier for our reading)

Serial.println("Initialization is done");

inittime = millis();
}


void loop() {
  
float moment = millis();
float diff = moment- inittime; // how much time passed from the initialization to the moment we are reading the variable

if (diff > 8000 && diff<9000 && !motorFired){
  Serial.println("FIRE...");
  digitalWrite(10, HIGH);   // (HIGH is the voltage level) = 5 [V]
  motorFired = 1;
  }
else {
      digitalWrite(10, LOW); // REOPEN the relay once firing complete (just for safety reasons)
  }
  
LoadCell.update(); // Read the load cell and convert into digital signals
float datacell = LoadCell.getData();
float datatime = millis(); // time stamps since the beginning of Arduino activity in [ms]

if(Serial.available()==0){

  Serial.print("Time = ");
  Serial.print(datatime);
  Serial.print("  -  Thrust = ");
  Serial.println(datacell);
}

}
