
clear
close

function dx = f( t , x )

//Features of the missile
M0 = 100; //kg
Is = 250; //s
T = 2000; // N

D = 0.35; //Missile diameter, m
L = 3.0 ; //Missile length, m
Sref = %pi * ( D / 2 )^2; //Reference surface area, m2

Xcg = 1.0; //Center of gravity from nose, m
Xcp = 1.8; //Center of pressure from nose, m  

Cd = 0.5; //Drag coefficient
dCnda = 2 .* %pi; //Derivative of the normal force coefficient w.r.t. alpha
delta = 0; // Thrust is aligned with the body axis


//constants
g0 = 9.81; // m/s2
rho= 1.25; // density, kg/m3

sqVeff = x( 2 )^2 + x( 4 )^2;
q = 0.5 * rho * sqVeff;

gam = atan( x( 4 ) / x( 2 ) ); //Flight path angle
alpha = x( 5 ) - gam; //Alpha derived from phi = x(5)

Cn = dCnda * alpha;

N = q * Sref * Cn; // Normal force
A = q * Sref * Cd; // Axial force

m = M0 - t * T / g0 / Is
Izz = 1 / 12 * m * L^2; 

Fx = T * cos( x( 5 ) + delta ) - A * cos( x( 5 ) ) - N * sin( x( 5 ) );
Fy = T * sin( x( 5 ) + delta ) - A * sin( x( 5 ) ) + N * cos( x( 5 ) ) - m * g0; 
Mzz = -N * ( Xcp - Xcg ) - T * sin( delta ) * ( L - Xcg );


dx( 1 ) = x( 2 );
dx( 2 ) = Fx / m;

dx( 3 ) = x( 4 );
dx( 4 ) = Fy / m;

dx( 5 ) = x( 6 );
dx( 6 ) = Mzz / Izz;

endfunction












//Initial conditions
x0( 1 ) = 0; //Starting position, m
x0( 2 ) = 10; // Initial velocity x, m/s
x0( 3 ) = 0; //Starting position, m 
x0( 4 ) = 10; // Initial velocity y, m/s
x0( 5 ) = 0.87; // Initial angle y, rad
x0( 6 ) = 0; // Initial angular velocity y, rad/s



t0 = 0;

t = 0 : 0.01 : 10;

y = ode( x0 , t0 , t , f ); 





