close all
clear all
set(0,'defaultTextInterpreter','latex')
set(0,'DefaultAxesFontSize', 14);


%% SIMULINK SOLVER SETUP
sim_time = 10; %[s]
max_dt = .01; %[s]
abs_tol = 1e-7;
rel_tol = 1e-7;
solver_name = 'ode45'

active_nutation_damping_v2
set_param('active_nutation_damping_v2', 'Solver', solver_name,...
    'MaxStep', num2str(max_dt), 'AbsTol', num2str(abs_tol), 'RelTol', num2str(rel_tol),...
    'StopTime', num2str(sim_time))

%% RUN SIMULINK MODEL
simOut = sim('active_nutation_damping_v2')
simOut.getSimulationMetadata.ModelInfo.SolverInfo

%% PLOT
% Numerical Solution
plot(simOut.tout, simOut.omega.Data(:,1:3))
legend({'$\omega_x$', '$\omega_y$', '$\omega_z$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('$\omega$ [rad/s]', 'Interpreter', 'latex')