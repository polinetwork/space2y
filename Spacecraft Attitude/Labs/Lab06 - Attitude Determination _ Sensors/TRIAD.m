function [A_BN] = TRIAD(p,q, a,b)

s1 = p;
s2 = cross(p,q) / norm(cross(p,q));
s3 = cross(p,s2);

v1 = a;
v2 = cross(a,b) / norm(cross(a,b));
v3 = cross(a,v2);

S = [s1 s2 s3];
V = [v1 v2 v3];

A_BN = S*V';


end

