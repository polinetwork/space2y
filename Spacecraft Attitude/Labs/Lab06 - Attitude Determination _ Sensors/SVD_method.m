function A_BN = SVD_method(v_B1, v_B2, v_N1, v_N2, alpha_1, alpha_2)

B = alpha_1*v_B1*v_N1' + alpha_2*v_B2*v_N2';

[U,~,V] = svd(B);
M = diag([1, 1, det(U)*det(V)]);
A_BN = U*M*V';

end

