close all
clear all
set(0,'defaultTextInterpreter','latex')
set(0,'DefaultAxesFontSize', 14);


%% SIMULINK SOLVER SETUP
sim_time = 10
max_dt = .01; %[s]
abs_tol = 1e-7;
rel_tol = 1e-7;
solver_name = 'ode113'

attitude_determination
set_param('attitude_determination', 'Solver', solver_name,...
    'MaxStep', num2str(max_dt), 'AbsTol', num2str(abs_tol), 'RelTol', num2str(rel_tol),...
    'StopTime', num2str(sim_time))

%% RUN SIMULINK MODEL
simOut = sim('attitude_determination')
simOut.getSimulationMetadata.ModelInfo.SolverInfo



%% PLOT
A_BN_sens = simOut.attitude.Data;
A_size = size(A_BN_sens);
A_unrolled = reshape(A_BN_sens, A_size(1)*A_size(2),A_size(3));
figure
plot(simOut.tout, A_unrolled)
legend({'$A_{11}$', '$A_{12}$', '$A_{13}$',...
    '$A_{21}$', '$A_{22}$', '$A_{23}$',...
    '$A_{31}$','$A_{32}$', '$A_{33}$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('Attitude matrix cosine [-]', 'Interpreter', 'latex')