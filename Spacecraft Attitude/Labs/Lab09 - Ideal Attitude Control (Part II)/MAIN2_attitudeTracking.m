close all
clear all
set(0,'defaultTextInterpreter','latex')
set(0,'DefaultAxesFontSize', 14);


%% SIMULINK SOLVER SETUP
sim_time = 30
max_dt = .1; % [s]
abs_tol = 1e-3;
rel_tol = 1e-3;
solver_name = 'ode15s'

attitudeTracking
set_param('attitudeTracking', 'Solver', solver_name,...
    'MaxStep', num2str(max_dt), 'AbsTol', num2str(abs_tol), 'RelTol', num2str(rel_tol),...
    'StopTime', num2str(sim_time))



%% RUN SIMULINK MODEL
simOut = sim('attitudeTracking')
simOut.getSimulationMetadata.ModelInfo.SolverInfo
