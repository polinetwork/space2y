close all
clear all
set(0,'defaultTextInterpreter','latex')
set(0,'DefaultAxesFontSize', 14);


%% SIMULINK SOLVER SETUP
sim_time = 100;
max_dt = .01; %[s]
abs_tol = 1e-7;
rel_tol = 1e-7;
solver_name = 'ode45'

pointing_error_quaternions
set_param('pointing_error_quaternions', 'Solver', solver_name,...
    'MaxStep', num2str(max_dt), 'AbsTol', num2str(abs_tol), 'RelTol', num2str(rel_tol),...
    'StopTime', num2str(sim_time))

%% RUN SIMULINK MODEL
simOut = sim('pointing_error_quaternions')
simOut.getSimulationMetadata.ModelInfo.SolverInfo



%% PLOT
q = simOut.q.Data;
A = quat2dcm(q);
A_size = size(A);
A_unrolled = reshape(A, A_size(1)*A_size(2),A_size(3));

figure
subplot(2,1,1)
plot(simOut.tout, A_unrolled)
legend({'$A_{11}$', '$A_{12}$', '$A_{13}$',...
    '$A_{21}$', '$A_{22}$', '$A_{23}$',...
    '$A_{31}$','$A_{32}$', '$A_{33}$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('Attitude matrix cosine [-]', 'Interpreter', 'latex')

subplot(2,1,2)
plot(simOut.tout, q)
legend({'$q_1$', '$q_2$', '$q_3$', '$q_4$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('Quaternions [-]', 'Interpreter', 'latex')
