close all
clear all
set(0,'defaultTextInterpreter','latex')
set(0,'DefaultAxesFontSize', 14);


%% SIMULINK SOLVER SETUP
sim_time = 100;
max_dt = .01; %[s]
abs_tol = 1e-7;
rel_tol = 1e-7;
solver_name = 'ode45'

error_wrt_MovingFrame
set_param('error_wrt_MovingFrame', 'Solver', solver_name,...
    'MaxStep', num2str(max_dt), 'AbsTol', num2str(abs_tol), 'RelTol', num2str(rel_tol),...
    'StopTime', num2str(sim_time))

%% RUN SIMULINK MODEL
simOut = sim('error_wrt_MovingFrame')
simOut.getSimulationMetadata.ModelInfo.SolverInfo

%% PLOT

plot(simOut.tout,simOut.q_BL.Data)
legend({'$q^{B/L}_1$', '$q^{B/L}_2$', '$q^{B/L}_3$', '$q^{B/L}_4$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('Attitude Error', 'Interpreter', 'latex')

