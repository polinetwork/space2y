function [sys,x0,str,ts] = dynamics(t,x,u,flag)

% dynamics
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 4;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 4;
sizes.NumInputs      = 6;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);

x0  = [0.2 0.3 0.1 30]';

str = [];
ts  = [0 0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)



%define variables and parameters
I1 = 1.65916e05; %[kg*m^2]
I2 = 1.65916e05; %[kg*m^2]
I3 = 2.2e04; %[kg*m^2]
Ir = 3.5e02; %[kg*m^2]


w(1,1) = x(1);   
w(2,1) = x(2);
w(3,1) = x(3);
w(4,1) = x(4);

   
%% Equations 



wdot = zeros(4,1);

wdot(1) = ((I2-I3)/I1)*x(2)*x(3) + u(1,1)/I1 + u(4,1)/I1 - Ir*x(4)*x(2)/I1;
wdot(2) = ((I3-I1)/I2)*x(1)*x(3) + u(2,1)/I2 + u(5,1)/I2 + Ir*x(4)*x(1)/I2;
wdot(3) = ((I1-I2)/I3)*x(2)*x(1) + u(3,1)/I3 + u(6,1)/I3;
wdot(4)=0;

sys = [wdot];

% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)

sys = x;


% end mdlOutputs