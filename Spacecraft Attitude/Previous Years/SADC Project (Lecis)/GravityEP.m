function [sys,x0,str,ts] = Gravity_gradient(t,x,u,flag)

% Gravity gradient torque
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 3;
sizes.NumInputs      = 9;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);

x0  = []';

str = [];
ts  = [0  0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)


sys = [];

% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)

A_BL(1,1) = u(1);
A_BL(1,2) = u(2);
A_BL(1,3) = u(3);
A_BL(2,1) = u(4);
A_BL(2,2) = u(5);
A_BL(2,3) = u(6);
A_BL(3,1) = u(7);
A_BL(3,2) = u(8);
A_BL(3,3) = u(9);

%Orbit characteristic
mu =398600; %[Km^3s^-2]
R = 550+6371; %[Km]
n = sqrt(mu/R^3); %[Km/s]

Ixx = 1.65916e05; %[kg*m^2]
Iyy = 1.65916e05; %[kg*m^2]
Izz = 2.2e04; %[kg*m^2]


c = A_BL*[1 0 0]'; 


%Gravity field torque
T_GFx = (3*mu/(R^3))*((Izz-Iyy)*c(3)*c(2));
T_GFy = (3*mu/(R^3))*((Ixx-Izz)*c(3)*c(1));
T_GFz = (3*mu/(R^3))*((Iyy-Ixx)*c(1)*c(2));

sys = [T_GFx T_GFy T_GFz]'; 


% end mdlOutputs