function [sys,x0,str,ts] = Drag(t,x,u,flag)
 
% nonlinear pendulum
%
% -------------------------------------------------------------------------
 
 
%%
switch flag,
 
  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;
 
  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);
 
  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);
 
  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];
 
  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);
 
end
 
 
%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes
 
sizes = simsizes;
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 3;
sizes.NumInputs      = 9;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;
 
sys = simsizes (sizes);
 
x0 = [];
str = [];
ts  = [0 0];
 
 
%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)
 
 
% end mdlDerivatives
 
%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================
 
 
function sys=mdlOutputs (t,x,u)
 
A_BN(1,1) = u(1);
A_BN(1,2) = u(2);
A_BN(1,3) = u(3);
A_BN(2,1) = u(4);
A_BN(2,2) = u(5);
A_BN(2,3) = u(6);
A_BN(3,1) = u(7);
A_BN(3,2) = u(8);
A_BN(3,3) = u(9);

%area of the spacecraft
%sides lengths
L_x = 13; %[m]
L_y = 13; %[m]
L_z = 4; %[m]

%Areas of the spacecraft
A_x = L_y*L_z; %[m^2]
A_y = L_x*L_z; %[m^2]
A_z = pi*(L_z/2)^2; %[m^2]

%Normal vector of the surfaces
N_xp = [1 0 0]';  %x_normal
N_yp = [0 1 0]';  %y_normal
N_zp = [0 0 1]';  %z_normal

N_xn = -N_xp;  %(minus)x_normal
N_yn = -N_yp;  %(minus)y_normal
N_zn = -N_zp;  %(minus)z_normal

%Definition of the pressure centre (10% around the centre of mass CM)
C_p =[0.1*L_x 0.1*L_y 0.1*L_z]';


%Orbit characteristic
w_e = 0.000072921; %[rad/s]
mu_Earth =398600*10^9; %[m^3s^-2]
R_orbit = (550+6371)*10^3; %[m]
n = sqrt(mu_Earth/R_orbit^3);
i = 28.47*pi/180;

%Position vector in body frame
r0_x = R_orbit*cos(n*t);  
r0_y = R_orbit*sin(n*t)*cos(i);
r0_z = R_orbit*sin(n*t)*sin(i);
r0 = [r0_x; r0_y; r0_z];


%Velocity vector in body frame
v_x = -R_orbit*n*sin(n*t) + w_e*r0_y;
v_y = R_orbit*n*cos(n*t)*cos(i) - w_e*r0_x;
v_z = R_orbit*n*cos(n*t)*sin(i);
v = [v_x; v_y; v_z];
vm = norm(v);

vb = A_BN*v;
vbm=vb/norm(vb);

%Drag coefficient and Density
Cd = 2.2; 
rho =3.059e-13; %[kg/m^3]



%Drag force

if N_xp'*vb >0
   F_x = (N_xp'*vbm)*A_x*C_p; %positive x_direction
   T_dx = cross(-0.5*rho*Cd*vm^2*vbm,F_x);
else 
   F_x = (N_xn'*vbm)*A_x*C_p; %negative x_direction 
   T_dx = cross(-0.5*rho*Cd*vm^2*vbm,F_x);
end


if N_yp'*vb >0
    F_y = (N_yp'*vbm)*A_y*C_p; %positive y_direction
    T_dy = cross(-0.5*rho*Cd*vm^2*vbm,F_y);
else
    F_y = (N_yn'*vbm)*A_y*C_p; %negative y_direction
    T_dy = cross(-0.5*rho*Cd*vm^2*vbm,F_y);
end


if N_zp'*vb >0
    F_z = (N_zp'*vbm)*A_z*C_p; %positive z_direction
    T_dz = cross(-0.5*rho*Cd*vm^2*vbm,F_z);
else 
    F_z = (N_zn'*vbm)*A_z*C_p; %negative z_direction
    T_dz = cross(-0.5*rho*Cd*vm^2*vbm,F_z);
end

%total drag acting on the spacecraft
T_D = T_dx+T_dy+T_dz; 
y(1) = T_D(1);
y(2) = T_D(2);
y(3) = T_D(3);

sys = y;
 
 
% end mdlOutputs