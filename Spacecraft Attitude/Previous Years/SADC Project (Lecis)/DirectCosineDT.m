function [sys,x0,str,ts] = Kinematics(t,x,u,flag)
 
% nonlinear pendulum
%
% -------------------------------------------------------------------------
 
 
%%
switch flag,
 
  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;
 
  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);
 
  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);
 
  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];
 
  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);
 
end
 
 
%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes
 
sizes = simsizes;
sizes.NumContStates  = 9;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 9;
sizes.NumInputs      = 3;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;
 
sys = simsizes (sizes);
 
x0 = [ 1 0 0 0 1 0 0 0 1 ];  %satellite starting position in direct cosine matrix
%  x0 = [0.9511 0.1341 0.2784 0 0.8569 -0.4126 0 0.4339 0.9010];
str = [];
ts  = [0 0];
 
 
%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)
 
A(1,1) = x(1);
A(1,2) = x(2);
A(1,3) = x(3);
A(2,1) = x(4);
A(2,2) = x(5);
A(2,3) = x(6);
A(3,1) = x(7);
A(3,2) = x(8);
A(3,3) = x(9);
 
%define variables and parameters
 
a_dot=zeros(9,1);
a_dot(1) = u(3)*x(4)-u(2)*x(7);
a_dot(2) = u(3)*x(5)-u(2)*x(8);
a_dot(3) = u(3)*x(6)-u(2)*x(9);
a_dot(4) = u(1)*x(7)-u(3)*x(1);
a_dot(5) = u(1)*x(8)-u(3)*x(2);
a_dot(6) = u(1)*x(9)-u(3)*x(3);
a_dot(7) = u(2)*x(1)-u(1)*x(4);
a_dot(8) = u(2)*x(2)-u(1)*x(5);
a_dot(9) = u(2)*x(3)-u(1)*x(6);

sys = [a_dot];
 
 
 
% end mdlDerivatives
 
%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================
 
 
function sys=mdlOutputs (t,x,u)
 
sys = x;
 
 
% end mdlOutputs