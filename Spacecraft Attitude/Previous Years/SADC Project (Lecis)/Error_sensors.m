%Magnetometer and Earth_sensor error

%This script is addressed to evaluate the standard deviation of sensor

A_BN0= [0.5335 0.8080 0.2500; -0.8080 0.3995 0.4330; 0.2500 -0.4300 0.8660]; 


%Measurements of the SUN and EARTH in inirtial frame 
%h Is the measurements vector for the Earth sensor
%s Is the measurements vector for the Sun sensor

h_Earth_n =[0.2673 0.5345 0.8018]';
M_n =[-0.3124 0.9370 0.1562]';

%perfect measures of the sensors in body frame

h_Earth_bp = A_BN0*h_Earth_n;
M_bp = A_BN0*M_n;

%error matrix which angle is done by the degree of accuracy of the sensor
%errors in radians of the Earth sensor

alpha = 0.1*(pi/180);

%errors in radians of the Magnetometer
beta = 0.5*(pi/180);

%the maximum error matrix for the Earth sensor
A_eps_E=[cos(alpha)*cos(alpha) -cos(alpha)*sin(alpha) sin(alpha);
    cos(alpha)*sin(alpha)*sin(alpha)+sin(alpha)*cos(alpha) -sin(alpha)*sin(alpha)*sin(alpha)+cos(alpha)*cos(alpha) -cos(alpha)*sin(alpha);
    -cos(alpha)*sin(alpha)*cos(alpha)+sin(alpha)*sin(alpha) sin(alpha)*sin(alpha)*cos(alpha)+sin(alpha)*cos(alpha) cos(alpha)*cos(alpha)];


%the maximum error matrix for the Magnetometer
A_eps_M=[cos(beta)*cos(beta) -cos(beta)*sin(beta) sin(beta);
    cos(beta)*sin(beta)*sin(beta)+sin(beta)*cos(beta) -sin(beta)*sin(beta)*sin(beta)+cos(beta)*cos(beta) -cos(beta)*sin(beta);
    -cos(beta)*sin(beta)*cos(beta)+sin(beta)*sin(beta) sin(beta)*sin(beta)*cos(beta)+sin(beta)*cos(beta) cos(beta)*cos(beta)];


%measures affected by the error of sensor

h_Earth_be = A_eps_E*h_Earth_bp;      %worst case sensor reading for the Earth Sensor
M_be = A_eps_M*M_bp;          %worst case sensors reading for the Magnetometer


%TRIAD method to evaulate A_BN
v3_n = cross(h_Earth_n,M_n)/norm(cross(h_Earth_n,M_n));     %third vector in intertial refenrence

v3_e=cross(h_Earth_be,M_be )/norm(cross(h_Earth_be,M_be )); %third vector with measures affected by errors

%matrix whith inertial measurement 
V_n = [h_Earth_n v3_n  cross(h_Earth_n ,v3_n)/norm(cross(h_Earth_n ,v3_n))];

%matrix whith body measurements
V_b =[h_Earth_be v3_e cross(h_Earth_be,v3_e)/norm(cross(h_Earth_be,v3_e))];

%matrix of sensor measurements
A_s = V_b*V_n';

%Maximum error matrix and standard deviation matrix
A_error=A_s*A_BN0';


%matrix of standard deviation
sdmatrix=eye(3)-A_error

%standard deviation
sd_max= max(abs(sdmatrix));
sd = max(sd_max)