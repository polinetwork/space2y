function [sys,x0,str,ts] = Kinematics(t,x,u,flag)

% nonlinear pendulum
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 9;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 9;
sizes.NumInputs      = 3;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);

x0 = [ 0 0 0 0 0 0 0 0 0];  %satellite starting position in direct cosine matrix

str = [];
ts  = [0 0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)

A(1,1) = x(1);
A(1,2) = x(2);
A(1,3) = x(3);
A(2,1) = x(4);
A(2,2) = x(5);
A(2,3) = x(6);
A(3,1) = x(7);
A(3,2) = x(8);
A(3,3) = x(9);

%define variables and parameters

w_hat =([0,    -u(3), u(2); ...
        u(3),    0,  -u(1); ...
       -u(2),   u(1),  0 ]);

   
%s Kinematics of direct cosine

Adot = -w_hat*A;

aout = vertcat(Adot(1,1:3)', Adot(2,1:3)', Adot(3,1:3)');
sys = [aout];




% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)

sys = x;


% end mdlOutputs