function [sys,x0,str,ts] = Euler_eq(t,x,u,flag)

% nonlinear pendulum
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 3;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 3;
sizes.NumInputs      = 3;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);

xx0 = [10 -10 15]'; %satellite angular starting velocities
d0  = [0 0 0]'; % w_d 
x0 = xx0 +d0;

str = [];
ts  = [0 0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)



%define variables and parameters


I1= 0.0109;
I2= 0.0504;
I3= 0.0506;

c1 = ((I2-I3)/I1);
c2 = ((I3-I1)/I2);
c3 = ((I1-I2)/I3);
w(1) = x(1);
w(2) = x(2);
w(3) = x(3);

wdot = zeros(3,1);
wdot(1) = c1*w(2)*w(3)+(u(1)/I1);
wdot(2) = c2*w(1)*w(3)+(u(2)/I2);
wdot(3) = c3*w(2)*w(1)+(u(3)/I3);

sys = [wdot];




% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)

sys = x;


% end mdlOutputs