function [sys,x0,str,ts] = RW_controller(t,x,u,flag)

% nonlinear pendulum
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 3;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 9;
sizes.NumInputs      = 6;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);
x0=[0 0 0]';



str = [];
ts  = [0 0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)
A= [1 0 0; 0 1 0; 0 0 1];
h(1) = x(1);
h(2) = x(2);
h(3) = x(3);
h = h';
ud(1) = u(1);
ud(2) = u(2);
ud(3) = u(3);
ud = ud';
w(1) = u(4);
w(2) = u(5);
w(3) = u(6);
w = w';
hdot = pinv(A)*(cross(A*h,w)-ud);
sys = [hdot];




% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)
A= [1 0 0; 0 1 0; 0 0 1];
h(1) = x(1);
h(2) = x(2);
h(3) = x(3);
h = h';
ud(1) = u(1);
ud(2) = u(2);
ud(3) = u(3);
ud = ud';
w(1) = u(4);
w(2) = u(5);
w(3) = u(6);
w = w';
hdot_out = pinv(A)*(cross(A*h,w)-ud);
sys = [h;hdot_out;w];


% end mdlOutputs