
close all
clear all
clc

opt = optimoptions(@ga,'PlotFcn', @gaplotbestf,'PopulationSize',100,'MaxGenerations',1000);
% -------------------------------------------------------------------------

% always decleare you objective function as anonymous @(x). the only
% variable shall be x. it can be a vector or a scalar
constant = 3;
single_objective_function = @(x) fun_peaks(x,constant);

% the optimization happens here
[x,fval,exitflag,output,population,scores] = ga(single_objective_function,2,[],[],[],[],[-3,-3],[3,3],[],[],opt);

% this is to show the topology
x_map = [linspace(-3,3,1000);linspace(-3,3,1000)];






