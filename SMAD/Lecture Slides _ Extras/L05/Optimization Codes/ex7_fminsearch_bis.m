
close all
clear all
clc


opt = optimset('PlotFcns',@optimplotfval,'Display','iter');
% -------------------------------------------------------------------------

% always decleare you objective function as anonymous @(x). the only
% variable shall be x. it can be a vector or a scalar
constant = 3;
single_objective_function = @(x) fun_peaks(x,constant);

x0 = [-1,-1];

% the optimization happens here
[x,fval,exitflag,output] = fminsearch(single_objective_function,x0,opt);

% this is to show the topology
x_map = [linspace(-3,3,1000);linspace(-3,3,1000)];
% always initialize your variables
Map = zeros(1000);
for i = 1:1000
    for j = 1:1000
        Map(j,i) = fun_peaks([x_map(1,i);x_map(2,j)],constant);
    end
end


figure()

surf(x_map(1,:),x_map(2,:),Map,'EdgeColor','none')
hold on
grid on
plot3(x0(1),x0(2),fun_peaks(x0,constant),'d')
plot3(x(1),x(2),fval,'+')
legend('Initial','Final')





