
close all
clear all
clc


% ---------- TRY TO AVOID GLOBAL VARIABLES IN YOUR CODES!!!!---------------
% I used global variables only to show internal optimization steps!!!!!'PlotFcn', @gaplotbestf
global history
outtfun = @(x,optimValues,state) outfun(x,optimValues,state);
opt = optimoptions(@ga,'PlotFcn', @gaplotbestf,'PopulationSize',1000,'MaxGenerations',3000);
% -------------------------------------------------------------------------

% always decleare you objective function as anonymous @(x). the only
% variable shall be x. it can be a vector or a scalar
constant = 3;
single_objective_function = @(x) fun_peaks_penalty(x,constant);


history.x = [];
history.fval = [];
history.searchdir = [];

% the optimization happens here
[x,fval,exitflag,output,population,scores] = ga(single_objective_function,2,[],[],[],[],[-3,-3],[3,3],[],[],opt);

% this is to show the topology
x_map = [linspace(-3,3,1000);linspace(-3,3,1000)];
% always initialize your variables
Map = zeros(1000);
for i = 1:1000
    for j = 1:1000
        Map(j,i) = fun_peaks_penalty([x_map(1,i);x_map(2,j)],constant);
    end
end





