function [fval] = fun_2obj(x)

fval(1) = x(1)^2 + x(2)^2;         % Paraboloid @(0,0)
fval(2) = (x(1)+1)^2 + (x(2)-1)^2; % Paraboloid @(-1,+1)

end

