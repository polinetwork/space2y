\begin{thenomenclature} 

 \nomgroup{A}

  \item [{$\hat{x}$}]\begingroup Parameter of interest's estimator \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$\phi$}]\begingroup Phase difference of SAR image \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$\sigma$}]\begingroup Standard deviation \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$\varepsilon$}]\begingroup Estimation error \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$N$}]\begingroup Number of realizations \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$R_{yx}$}]\begingroup Cross-correlation function \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$R_{yy}$}]\begingroup Auto-correlation function \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$w$}]\begingroup Phase noise \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$x$}]\begingroup Parameter of interest \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$y$}]\begingroup Measured signal \nomunit{-}\nomeqref {0}
		\nompageref{i}

 \nomgroup{B}

  \item [{$\tau$}]\begingroup Time lag \nomunit{s}\nomeqref {0}
		\nompageref{i}
  \item [{$f$}]\begingroup Frequency \nomunit{Hz}\nomeqref {0}
		\nompageref{i}
  \item [{$k_v$}]\begingroup Velocity-to-phase conversion factor \nomunit{\frac{day}{mm}}\nomeqref {0}
		\nompageref{i}
  \item [{$k_z$}]\begingroup Height-to-phase conversion factor \nomunit{\frac{1}{m}}\nomeqref {0}
		\nompageref{i}
  \item [{$S_x$}]\begingroup Power spectral density \nomunit{\frac{W}{Hz}}\nomeqref {0}
		\nompageref{i}
  \item [{$t$}]\begingroup Time \nomunit{s}\nomeqref {0}\nompageref{i}
  \item [{$v$}]\begingroup Surface displacement rate \nomunit{\frac{mm}{day}}\nomeqref {0}
		\nompageref{i}
  \item [{$z$}]\begingroup Terrain topography \nomunit{m}\nomeqref {0}
		\nompageref{i}

 \nomgroup{D}

  \item [{\acron{BLUE}{Best Linear Unbiased Estimator}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{CRLB}{Cramer-Rao Lower Bound}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{FFT}{Fast Fourier Transform}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{FIR}{Finite Impulse Response}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{IFFT}{Inverse Fast Fourier Transform}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{InPhase}{Interferometric Phase}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{MLE}{Maximum Likelihood Estimator}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{MVUE}{Minimum Variance Unbiased Estimator}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{PSD}{Power Spectral Density}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{RCS}{Radar Cross-Section}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{RMS}{Root Mean Square}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{SAR}{Synthetic Aperture Radar}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{w.r.t.}{with respect to}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{WSS}{Wide Sense Stationary}}]\begingroup \nomeqref {0}
		\nompageref{i}

\end{thenomenclature}
