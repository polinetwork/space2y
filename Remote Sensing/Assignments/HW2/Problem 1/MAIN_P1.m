close all
clear all
clc

% AGB = A * sigma_HV^p
A = 2.4e+6;
p = 2.6;

acc_AGB = 0.2; % [-] Relative accuracy of Above Ground Biomass
               %     (AGB) estimation
rho_x = 10; % [m] Azimuth resolution
f0 = 435e+6; % [Hz] Carrier frequency
B = 6e+6; % [Hz] Transmitted bandwidth
H = 666e+3; % [m] Orbital height
theta = deg2rad(28); % [rad] Reference incidence angle (off-nadir)
T_noise = 290; % [K] Reference noise temperature
AGB_min  = 250; % [ton/hectare] Minimum of the range of AGB values
AGB_max  = 500; % [ton/hectare] Maximum of the range of AGB values
sigmaHH = db2pow(-8); % [-] Backscattering coefficient
                       % for HH polarization
z_amb = 80; % [m] Height of ambiguity
                       
                       
%% 1. Derive the relative accuracy to be required on σhv to ensure an accuracy of 20% on AGB.
% Note: relative accuracy for a variable x is defined as sigma_x/x , where sigma_x is the standard deviation
% of the estimation error.
acc_sigmaHV = acc_AGB/p % [-] Relative accuracy of sigmaHV


%% 2. Determine the size of the averaging window to meet the requirement derived at point 1).
N_eq = (1 / acc_sigmaHV) ^ 2 % [-] Number of equivalent indipendent samples
T_avg = N_eq / B % [s] Duration of the averaging window


%% 3. Determine a), b), c) as necessary to ensure a Signal to Noise Ratio of at least 10 dB
%     at HV polarization across the whole range of AGB values.
%           a) Pulse Repetition Interval (PRI)
%           b) Duration of the transmitted pulses
%           c) Transmitted power for each transmitted pulse

T_obs = 1e-5; %[s] Pulse duration ***** DESIGN CHOICE *****


SNR = db2pow(10); % [-] Minimum SNR at HV polarization across the whole range of AGB values
c = 3e+8; % [m/s] Speed of light
lambda = c / f0; % [m] Carrier wavelength
Lx = 2*rho_x % [m] Antenna's azimuth-wise length
R = H / cos(theta); % [m] Reference height

% FoS = 2; % Factor of safety ***** ASSUMED *****
% PRI = FoS * 2*R/c % [s] Pulse Repetition Interval
% if T_obs > 0.7 * 2*R/c
%     error('T_obs is too long: almost equal, or even >, compared to ToF=2*R/c')
% end
mu = 398600; % [km^3/s^2] Gravitational parameter of Earth
R_earth = 6371.8; % [km] Earth's equatorial radius
v_orb = 1000 * sqrt(mu / (R_earth+H*1e-3)); % [m/s] Circular orbit velocity
dx_ant = Lx/2*0.5; % [m] Spatial sampling of the synthetic aperture
PRI = dx_ant/v_orb % [s] Pulse Repetition Interval


% HP: Noise Figure = 3dB (typical range of values 2-4)
F = db2pow(3); % [-] Noise figure ***** ASSUMED *****
K = 1.38*1e-23; % Boltzmann constant
Pn = K*T_noise*B*F % [W] Noise power

% Illuminated area
rho_R = c / (2*B) % Range resolution
delta_psi = lambda / Lx
A_ill = R * rho_R * delta_psi % [m^2] Illuminated area


% Received power
N0 = K*T_noise*F; % Noise spectral density
Er = N0 * SNR % [J] Received energy
Pr = Er / T_obs % [W] Received power


%  Transmitted power
Loss_atm = db2pow(3); %[-] Atmospheric losses ***** ASSUMED *****
ant_AR = 7; % [-] Antenna aspect ratio ***** ASSUMED *****
Lz = Lx / ant_AR % [m] Antenna's elevation-wise length
A_ant = Lx*Lz % [m^2] Antenna area
G = 4*pi/(lambda^2) * A_ant % [-] Antenna gain
Pt = Pr * (4*pi*R^2)^2 / (G/Loss_atm * A_ant * (AGB_min/A)^(1/p) * A_ill) % [W] Peak transmitted power


%% 4. Discuss whether your response at points 1) and 2) needs to be modified
%     when accounting for thermal noise.


%% 5. Discuss the choice of the focusing processor
%     (is 1D azimuth matched filtering enough?).

%% 6. One of BIOMASS secondary goals is to map terrain topography using SAR Interferometry.
%     By virtue of the penetration capabilities of P-Band waves, one can assume that the
%     signal in HH polarization is mostly contributed by trunk-ground double bounce scattering
%     from several tree trunks within any resolution cell. Accordingly, the phase center can be
%     (at least approximately) assumed to be directly associated with terrain topography.


      % a.) Evaluate the amount of phase noise in HH interferograms for a height of height of
      %     ambiguity z_amb = 80 m (recall that zamb = 2π/k_z).
     
RCS_HH = sigmaHH*A_ill;
Pr_HH = RCS_HH * Pt * G/Loss_atm * A_ant / (4*pi*R^2)^2

Er_HH = Pr_HH * T_obs;
SNR_HH = Er_HH / N0

% gamma_complex = SNR / (1+SNR) * exp(1i*4*pi/lambda * (Rm-Rm)); % [-] Interferometric coeherence
gamma = SNR_HH / (1+SNR_HH) % [-] Interferometric coherence (absolute value)

L = 1;
% [rad] Interferometric phase  dispersion
sigma_dphi_noAvg = sqrt(  (1-gamma^2) / (2*L*gamma^2)  )
      


      % b.) Determine the size of the averaging window required to estimate topography to
      %     within an accuracy of 5 m.
      
sigma_z = 5; % [m] Altitude dispersion

% [rad] Interferometric phase  dispersion
sigma_dphi = sigma_z * 2*pi/z_amb

% [-] Number of pixels in averaging window 
L_min = ceil( (1-gamma^2) / (2*sigma_dphi^2*gamma^2) )

      


