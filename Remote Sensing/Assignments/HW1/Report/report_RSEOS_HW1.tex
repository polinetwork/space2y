\input{mySettings}

%\includeonly{chapters/ch02}

\usepackage[english]{babel}


%% *************************** SPECIFIC NEWCOMMANDS ***************************
\newcommand{\parallelsum}{\mathbin{\!/\mkern-5mu/\!}}
% Parallel symbol


\newcommand*\circled[1]{\tikz[baseline=(char.base)]{%
            \node[circle=square,draw,inner sep=3pt] (char) {\textcolor{black}{#1}};}}
\newcommand*\rectangled[1]{\tikz[baseline=(char.base)]{%
            \node[rectangle=square,draw,inner sep=3pt] (char) {\textcolor{black}{#1}};}}

\newcommand{\example}[1]{\myhang{\circled{\textbf{\uline{Ex}}}\hspace{4ex}}#1}

\newcommand{\finding}[1]{\myhang{\rectangled{\textbf{\uline{RMK.:}}}\hspace{4ex}}#1}
\newcommand{\numof}[0]{n^{\circ}}

\newcommand{\restr}[2]{\left.#1\right|_{#2}}

\newcommand{\acron}[2]{\noindent\hbox to 0.125\textwidth{\textbf{#1}}#2}

\newcommand{\EV}[1]{E \brackg{#1}}

\newcommand{\estimator}[0]{\V{\hat{x}}}

\newcommand{\FT}[1]{\mathcal{F}\brackg{#1}}
\newcommand{\IFT}[1]{\mathcal{F}^{-1}\brackg{#1}}


%% *************************** SPECIFIC NEWCOMMANDS ***************************


\graphicspath{{img/}} % Set images directory




\begin{document}

\begin{center}
\includegraphics[width=0.2\textwidth]{logoPolimi_new} \\ \vspace{2ex}
\Huge{\textbf{\sffamily Homework Assignment No. 1}} \\ \Large{\emph{Remote Sensing for Earth Observation \& Surveillance}} \\ \vspace{2ex} \Large{Massimo Piazza} (919920) \\ \mailto{massimo.piazza@mail.polimi.it}
\end{center}


%\subsection*{\centering Abstract}
%\blindtext[1]
 

%\newpage
\let\cleardoublepage\clearpage % In order to avoid a blank page after nomenclature

\vspace{10ex}

\tableofcontents

\thispagestyle{empty}


\newpage

\frontmatter

\subsection*{\centering \huge{Nomenclature}}

\nomenclature[A]{$\phi$}{Phase difference of SAR image \nomunit{-}}
\nomenclature[A]{$w$}{Phase noise \nomunit{-}}
\nomenclature[A]{$\sigma$}{Standard deviation \nomunit{-}}
\nomenclature[A]{$x$}{Parameter of interest \nomunit{-}}
\nomenclature[A]{$\hat{x}$}{Parameter of interest's estimator \nomunit{-}}
\nomenclature[A]{$y$}{Measured signal \nomunit{-}}
\nomenclature[A]{$\varepsilon$}{Estimation error \nomunit{-}}
\nomenclature[A]{$N$}{Number of realizations \nomunit{-}}
\nomenclature[A]{$R_{yy}$}{Auto-correlation function \nomunit{-}}
\nomenclature[A]{$R_{yx}$}{Cross-correlation function \nomunit{-}}







\nomenclature[B]{$z$}{Terrain topography \nomunit{m}}
\nomenclature[B]{$v$}{Surface displacement rate \nomunit{\frac{mm}{day}}}
\nomenclature[B]{$k_z$}{Height-to-phase conversion factor \nomunit{\frac{1}{m}}}
\nomenclature[B]{$k_v$}{Velocity-to-phase conversion factor \nomunit{\frac{day}{mm}}}
\nomenclature[B]{$S_x$}{Power spectral density \nomunit{\frac{W}{Hz}}}
\nomenclature[B]{$\tau$}{Time lag \nomunit{s}}
\nomenclature[B]{$t$}{Time \nomunit{s}}
\nomenclature[B]{$f$}{Frequency \nomunit{Hz}}



%\nomenclature[C]{c}{Combustion Chamber}
%\nomenclature[C]{p}{Propellant}
%\nomenclature[C]{tot}{Total}

\nomenclature[D]{\acron{SAR}{Synthetic Aperture Radar}}{}
\nomenclature[D]{\acron{InPhase}{Interferometric Phase}}{}
\nomenclature[D]{\acron{BLUE}{Best Linear Unbiased Estimator}}{}
\nomenclature[D]{\acron{MLE}{Maximum Likelihood Estimator}}{}
\nomenclature[D]{\acron{MVUE}{Minimum Variance Unbiased Estimator}}{}
\nomenclature[D]{\acron{CRLB}{Cramer-Rao Lower Bound}}{}
\nomenclature[D]{\acron{FIR}{Finite Impulse Response}}{}
\nomenclature[D]{\acron{RMS}{Root Mean Square}}{}
\nomenclature[D]{\acron{PSD}{Power Spectral Density}}{}
\nomenclature[D]{\acron{FFT}{Fast Fourier Transform}}{}
\nomenclature[D]{\acron{IFFT}{Inverse Fast Fourier Transform}}{}
\nomenclature[D]{\acron{RCS}{Radar Cross-Section}}{}
\nomenclature[D]{\acron{WSS}{Wide Sense Stationary}}{}
\nomenclature[D]{\acron{w.r.t.}{with respect to}}{}



 \setlength{\columnsep}{30pt}
 \begin{multicols}{2}
 \printnomenclature
 \end{multicols}

\newpage

\mainmatter

\section{Estimation in SAR Interferometry}
\subsection{Introduction}
A SAR dataset is provided, containing information about the measured interferometric phase for each pixel of N=4 different radar images (Fig. \ref{fig:inPhaseImages}), taken by a satellite during successive passes above a given location on Earth's surface. Both the local topography and displacement-rate of the terrain can be estimated from the knowledge of $\phi$. In addition, the true values of height and velocity are provided as well, for the purpose of evaluating the performance of the estimation techniques that will be employed. The entire dataset is reported in Tab. \ref{tab:datasetP1}.

\begin{table}[H]
\centering
\caption{SAR dataset}
\label{tab:datasetP1}
\begin{tabular}{@{}ccc@{}}
\toprule
                 & \textbf{Variable Name} & \textbf{Variable Size} \\ \midrule \midrule
No. realizations & \texttt{N}              & 1x1                    \\
Pixels-x         & \texttt{Nx}             & 1x1                    \\
Pixels-y         & \texttt{Ny}             & 1x1                    \\
Coefficients-v   & \texttt{kv}             & 1x4                    \\
Coefficients-z   & \texttt{kz}             & 1x4                    \\
N phase images   & \texttt{phi}            & 650x1000x4             \\
Noise variance   & \texttt{sw}             & 1x1                    \\
True height      & \texttt{z}              & 650x1000               \\
True velocity    & \texttt{v}              & 650x1000               \\ \bottomrule
\end{tabular}
\end{table}


\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{phaseMeasurement_Realizations}
\caption{\label{fig:inPhaseImages} InPhase measurements}
\end{figure}


\subsection{Estimation of Topography and Displacement-Rate}
The interferometric phase associated with a given pixel might be reasonably modeled as:

\begin{equation}
\label{eq:InPhase}
\phi(n) = k_z(n) \cdot z + k_v(n) \cdot v + w(n) - w(0)
\end{equation}

which, given our set of N=4 realizations, can be rewritten in matrix form as:

\begin{equation}
\label{eq:InPhaseMatr}
  \underbrace{\colvec{\phi(1); \phi(2); \phi(3); \phi(4)}}_{\V{y}}
  =
\underbrace{ \begin{bmatrix}
k_z(1) & k_v(1) \\
k_z(2) & k_v(2) \\
k_z(3) & k_v(3) \\
k_z(4) & k_v(4)
\end{bmatrix} }_{\M{A}} \underbrace{\colvec{z; v}}_{\V{x}}
+ \underbrace{\colvec{w(1)-w(0); w(2)-w(0); w(3)-w(0); w(4)-w(0)}}_{\V{w}}
\end{equation}

The phase noise is modeled as a \textbf{zero-mean uncorrelated} stochastic process, i.e. $\EV{\V{w}} = \V{0}$,  $\M{C}_w = \text{diag}(\sigma^2,\dots,\sigma^2)$. In addition to this, since the problem is also linear, it can be derived that $\estimator\smallsub{BLUE}  \equiv \estimator\smallsub{MLE} \equiv \estimator\smallsub{MVU}$.\\
This will in turn lead to the choice of \textbf{BLUE} as the most suitable technique: it is the simplest estimator that achieves the CRLB\footnote{BLUE is in general a sub-optimal solution, obtained by seeking for an MVUE that is constrained to be linear}.

Without providing the derivation, which can be found in \cite{kay1993fundamentals}, the expression for the above mentioned estimator is given by
\begin{equation}
\label{eq:xBLUE}
  \estimator\smallsub{BLUE} = (\M{A}^\T \M{C}_w^{-1} \M{A})^{-1} \M{A}^\T  \M{C}_w^{-1} \, \V{y}
\end{equation}

Solving the linear problem defined in (\ref{eq:xBLUE}) for each of the $1000\times650$ pixels, produces radar images of the displacement and velocity fields that are inevitably corrupted by the presence of \emph{speckle} noise, as it can be clearly seen in Fig. \ref{fig:BLUE_estimation}.

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{BLUE_estimation}
\caption{\label{fig:BLUE_estimation} BLUE estimation}
\end{figure}

Noise removal has been carried out by means of a 20th-order lowpass \textbf{FIR} filter\footnote{it is a filter whose impulse response is of finite duration, i.e. it settles to zero in finite time}, with passband $\omega \in \brackg{0, \, 0.1\pi}$ \addunit{rad/sample}. The same filter has been used for processing the spectra in both the horizontal and vertical directions of the image. The resulting images will turn out to be \enquote{blurrier}, although definitely more physically sound, as it can be observed in Fig. \ref{fig:BLUE_estimationFiltered}.

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{BLUE_estimationFiltered}
\caption{\label{fig:BLUE_estimationFiltered} Despeckled BLUE estimation}
\end{figure}


\subsection{Estimation Performance}
\paragraph{Theoretical Evaluation}
The typical theoretical evaluation metric, within the framework of estimation theory, is the RMS of the error $\V{\varepsilon} := \estimator - \V{x}$, which, under the assumption of unbiasedness, can be computed as $\V{\text{\textbf{RMS}}}_\varepsilon\smallsup{th} = \text{diag}( \text{sqrt}( \M{C}_\varepsilon ))$, where
\begin{equation}
\label{eq:errorRMS}
\M{C}_\varepsilon\smallsup{BLUE} = (\M{A}^\T \M{C}_w^{-1} \M{A})^{-1}
\end{equation}

hence resulting in:
\[
\V{\text{\textbf{RMS}}}_\varepsilon\smallsup{th} = \colvec{35.698 \addunit{m};
          0.389 \addunit{mm/day}}
\]

One may also check that the matrix inversion is unbiased by verifying that, besides small numerical errors, the matrix multiplication $\M{A}^{-1}\M{A}$ yields the ($N \times N$) identity matrix, which indeed occurs:
\[
\M{A}^{-1}\M{A}
=
\begin{bmatrix}
\texttt{1} & \texttt{-1.421e-14} \\
\texttt{2.168-18} & \texttt{1} \\
\end{bmatrix}
\]


\paragraph{Empirical Evaluation}
The dataset also provides us with the ground truth (Fig. \ref{fig:groundTruth}), which can be compared against our estimation (we will take as a reference the filtered one), in order to accurately determine the estimation performance.

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{groundTruth}
\caption{\label{fig:groundTruth} Ground truth}
\end{figure}

Let us now compute the empirical RMS of the error, along with the expected value of its magnitude (i.e. the sample average over all pixels):

\[
\V{\text{\textbf{RMS}}}_\varepsilon = \colvec{ \sqrt{\texttt{mean}((\M{\hat{Z}}-\M{Z})^2)}; \sqrt{\texttt{mean}((\M{\hat{V}}-\M{V})^2)}} = \colvec{64.364 \addunit{m}; 1.118 \addunit{mm/day}}
\]
\[
\V{\mu}_{|\varepsilon|} = \colvec{ \texttt{mean}(|\M{\hat{Z}}-\M{Z}|); \texttt{mean}(|\M{\hat{V}}-\M{V}|)} = \colvec{5.565 \addunit{m}; 0.079 \addunit{mm/day}}
\]
where $\M{Z}$ and $\M{V}$ denote the matrices of displacement and velocity, respectively.

In addition, it is also evident the advantage of filtering the image, which reduces the errors by an order of magnitude, compared to the raw BLUE estimation:
\[
\V{\mu}_\varepsilon\smallsup{unfiltered} = \colvec{51.365 \addunit{m}; 0.892 \addunit{mm/day}}
\]

It might also be of interest to visualize the local error:

\begin{figure}[H]
\centering
\includegraphics[width=.8\textwidth]{localError}
\caption{\label{fig:localError} Local error}
\end{figure}


As it can be clearly observed in Fig. \ref{fig:localError}, the error is practically just noise, which graphically confirms the unbiasedness of our estimation. One may also formally verify such unbiasedness by computing the expected value of the relative error, that has been properly normalized as follows:

\[
\V{\mu}_{\varepsilon_\mathrm{rel}} = \colvec{ \displaystyle{\frac{\texttt{mean}(\M{\hat{Z}}-\M{Z})}{\sigma\smallsub{Z}}}; \displaystyle{\frac{\texttt{mean}(\M{\hat{V}}-\M{V})}{\sigma\smallsub{V}}}} = \colvec{ -5.522 \%;  +0.167 \%}
\]





\newpage

\section{Radar Target Detection}
The working principle of a passive bi-static radar is that of exploiting the signal of any large-bandwidth source of opportunity (e.g. the one emitted by a Digital-TV broadcasting antenna). The signal measured by the receiver will thus be given by the emitted signal superposed with the echoes of the latter, scattered by targets on the ground that we are willing to detect.\\
The received signal might then be reasonably modeled as:

\begin{equation}
\label{eq:receivedSignal}
y(t) = x(t) + \sum_{p=1}^{P} A_p x(t-\tau_p)
\end{equation}
where $x(t)$ denotes the emitted signal, $P$ is the number of ground-targets, while $A_p$ is a generic amplitude coefficient. The delays $\tau_p$ will clearly depend upon the length of the travel path of each echo (emitter $\rightarrow$ target $\rightarrow$ receiver).

The dataset provided for the upcoming processing is reported in Tab. \ref{tab:datasetP2}.

\begin{table}[H]
\centering
\caption{Passive radar dataset}
\label{tab:datasetP2}
\begin{tabular}{@{}ccc@{}}
\toprule
                 & \textbf{Variable Name} & \textbf{Variable Size} \\ \midrule \midrule
Emitted signal 		& \texttt{x}             & 19609x1                \\
Received signal     & \texttt{y}             & 19609x1                \\
Sampling time       & \texttt{dt}            & 1x1                    \\
\bottomrule
\end{tabular}
\end{table}



Under the assumption of dealing with an \textbf{ergodic} random process,\footnote{i.e. whose statistics can be recovered from a single (sufficiently long) realization} the PSD of the latter can be estimated as

\begin{equation}
\label{eq:}
  \hat{S}_x = \frac{|X(f)|^2}{T_|obs|}
\end{equation}

where $X(f) = \FT{x(t)}$.


As it can be immediately inferred from the spectral analysis in Fig. \ref{fig:directAndReceivedSignals}, $x(t)$ can be modeled as a complex stochastic process, white in the frequency band $[-3,+3] \cdot 10^7 \addunit{Hz} $.
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{directAndReceivedSignals}
\caption{\label{fig:directAndReceivedSignals} Spectral analysis of direct and received signals}
\end{figure}



\subsection{Target Detection}


\subsubsection{Detection Based on the Knowledge of $x(t)$ and $y(t)$}

Assuming that both the direct and the received signals are available for processing purposes, we might proceed by computing their \textbf{cross-correlation} that, under the assumption of a WSS process, becomes a function of the time lag only:
\begin{equation}
\label{eq:crossCorrel_def}
R_{yx}(\tau) := \EV{x(t) \cdot y^*(t+\tau)}  
\end{equation}

This will result in a signal characterized by a certain number of peaks in correspondence of each $\tau = \tau_p$, i.e. one peak per target. Such a trend is due to the fact that any white process, such as $x(t)$, is completely uncorrelated. Thus it will exhibit an auto-correlation that is basically zero for any time lag (except of course in correspondence of $\tau = 0$).\\
In order to understand the meaning of $R_{yx}(\tau)$ it has to be considered that $y(t)$ is given by the superposition of $x(t)$ itself with a few other echoes. The latter will exhibit a linear dependence upon something already happened in the past history of $x(t)$, thus resulting in a sudden correlation peak whenever an echo is detected.\\

It may also be derived that the cross-PSD function can be computed as the Fourier transform of the cross-correlation, which leads to the following numerical implementation:

\begin{equation}
\label{eq:crossCorrel_comp}
\begin{split}
R_{yx}(\tau) &= \IFT{S_{yx}(f)}\\
	   &= \frac{1}{T_|obs|} \, \IFT{Y(f) X^{*}(f)}\\
	   &\equiv \frac{1}{T_|obs|} \texttt{IFFT}( \,\, \texttt{FFT}(y(t)) \, \cdot \,\texttt{conj}(\texttt{FFT}(x(t))) \,\, )
\end{split}
\end{equation}

In addition, we will actually consider a scaled version of the cross-correlation, that has been computed as:

\begin{equation}
\label{eq:crossCorrel_norm}
  R_{yx}\smallsup{scaled}(\tau) = \frac{ R_{yx}(\tau) } { \sqrt{ \sum_i |x_i|^2 \cdot \sum_i |y_i|^2 } }
\end{equation}


\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{cross_correl_xy}
\caption{\label{fig:cross_correl_xy} Scaled cross-correlation between received and direct signals}
\end{figure}

Three distinct peaks are clearly visible in Fig. \ref{fig:cross_correl_xy}, in correspondence of $\tau_p = \{ 1.893, 1.993, 2.213 \}\addunit{\mu s}$.



\vspace{4ex}
Note that the presence of two consecutive peaks that are quite close to each other does not necessarily mean that the targets are spatially close. Indeed, there is a chance that their relative distance from the receiver is similar, although the two targets are distant from one another.\\
In addition, it can be intuitively concluded that the minimum temporal separation between two peaks required to resolve separate targets is given by \uline{twice the sampling time}. A peak occurring in two successive samples might in principle be caused either by the same target or by separate ones.\\

For what concerns the estimation of the amplitude coefficients, this might be carried out by moving to the frequency domain:


\[
y(t) = x(t) + \sum_{p=1}^{P} A_p x(t-\tau_p) 
\xmapsto[]{\mathrm{FFT}}
Y(f) = X(f) + \sum_{p=1}^{P} A_p X(f) e^{-j2\pi f \tau_p}
\]

\begin{equation}
\label{eq:echo_freq}
\Longrightarrow
\underbrace{A_p e^{-j2\pi f \tau_p}}_{\displaystyle:=E(f)} = \frac{Y(f)}{X(f)} - 1
\end{equation}


The information about the amplitude coefficients is immediately available from the Fourier anti-transform of $E(f)$, indeed:

\begin{equation}
\begin{split}
\label{eq:echo_time}
  e(t) &\equiv A_p \cdot \delta(t-\tau_p) \\
  &= \IFT{E(f)}
\end{split}
\end{equation}

In other words, by computing the IFT of $E(f)$ as defined in (\ref{eq:echo_freq}), we will end up with a signal characterized by peaks of intensity $A_p$ in correspondence of each $\tau_p$, as it can be clearly observed by the naked eye in Fig. \ref{fig:echo_time}.

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{echo_time}
\caption{\label{fig:echo_time} Received echo from scattering targets}
\end{figure}

The $A_p$ coefficients will be related to the size of the RCS of ground targets. The aforementioned peaks are perfectly consistent with the results obtained in Fig. \ref{fig:cross_correl_xy}.
Specifically, we will have:

\begin{table}[H]
\centering
\caption{Lags and corresponding amplitude coefficients}
\label{tab:lags_and_amplitudes}
\begin{tabular}{@{}cc@{}}
\toprule
\begin{tabular}[c]{@{}c@{}}$\tau_p$ $\mathrm{[\mu s]}$ \end{tabular} & \begin{tabular}[c]{@{}c@{}}$A_p$ $\mathrm{[-]}$ \end{tabular} \\ \midrule \midrule
1.893                                                                & 0.616                                                         \\
1.993                                                                & 0.618                                                         \\
2.213                                                                & 0.600                                                        \\ \bottomrule
\end{tabular}
\end{table}




\subsubsection{Detection Based on the Knowledge of $y(t)$ Only}
If instead the direct signal is not available, which is indeed a more realistic scenario of real-time processing (e.g. onboard an aircraft), it is still possible to accurately estimate the echoes' relative travel time w.r.t. the direct path.\\
Based on a reasoning similar to the previous one, computing the \textbf{auto-correlation} function $R_{yy}(\tau)$ will yield a signal characterized by peaks in correspondence of the $\tau_p$'s.

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{auto_correl_yy}
\caption{\label{fig:auto_correl_yy} Auto-correlation of the received signal}
\end{figure}


In Fig. \ref{fig:auto_correl_yy} the same exact peaks detected in Fig. \ref{fig:cross_correl_xy} are obtained, which further validates the chosen strategy.


\subsection{Effect of Reducing the Processed Data Volume}
We will now investigate the effect of reducing the amount of processed data to just the first 800 samples (which still contain the echoes we are willing to detect). This might for instance be a constraint due to computational power limitations, required for real-time processing.\\

From an initial quick spectral analysis, it appears that some problems may arise, due to a considered realization that is not sufficiently long.

\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{directAndReceivedSignals_red}
\caption{\label{fig:directAndReceivedSignals_red} Spectral analysis of the truncated direct and received signals}
\end{figure}

Comparing Figs. \ref{fig:directAndReceivedSignals_red} and \ref{fig:directAndReceivedSignals} it is evident that $x(t)$ no longer resembles a white process, although its spectral content still lies within the same frequency band.

\subsubsection{Detection Based on the Knowledge of $x(t)$ and $y(t)$}
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{cross_correl_xy_red}
\caption{\label{fig:cross_correl_xy_red} Cross-correlation between the truncated signals}
\end{figure}


As highlighted by Fig. \ref{fig:cross_correl_xy_red}, even though $R_{yx}\smallsup{red}(\tau)$ appears to have higher variance, compared to Fig. \ref{fig:cross_correl_xy}, the correct location of the $\tau_p$'s is perfectly identified.


\subsubsection{Detection Based on the Knowledge of $y(t)$ Only}
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{auto_correl_yy_red}
\caption{\label{fig:auto_correl_yy_red} Auto-correlation of the truncated received signal}
\end{figure}

By comparing Figs. \ref{fig:auto_correl_yy_red} and \ref{fig:auto_correl_yy}, it can be seen that the variance of the autocorrelation becomes extremely larger, making it no longer possible to accurately identify the target's delay.



\newpage

\nocite{*} % IMPORTANT: without this command, bib items which aren't already cited won't show up within your "References"

\printbibliography

\thispagestyle{empty}

\end{document}