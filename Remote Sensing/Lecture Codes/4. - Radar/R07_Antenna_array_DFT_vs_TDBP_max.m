clear all
close all
clc

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% what happens when spacing between neighboring antennas (dx_ant) is too large?
% what changes with array length?
% what changes with central frequency?
% what happens in projected images (XY) when focusing at a different
% height?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% RADAR PARAMETERS
c = 3e8; % speed of light [m/s]
f0 = 1e10; % carrier frequency [Hz]
lambda = c/f0 %wavelength [m]
B = 80e6; % bandwidth [Hz]
rho_r = c/2/B % range resolution [m]
delta_psi = 90/180*pi; % antenna beamwidth in azimuth [rad]
delta_teta = 30/180*pi; % antenna beamwidth in elevation rad]
Ax = 2; % array length [m]
% antenna positions
fx_max = 2/lambda*sin(delta_psi/2);
dx_ant = 1/(2*fx_max) % spacing between neighboring antennas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GEOMETRY
H = 100; % height of the array (e.g.: on top a tower or a building) [m]
theta0 = 45/180*pi; % average incidence angle [rad]
theta_min = theta0 - delta_teta/2; % max and min incidence angle
theta_max = theta0 + delta_teta/2;
r_min = H/cos(theta_min); % minimum and max distances
r_max = H/cos(theta_max);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ARRAY
N = 2*round(Ax/2/dx_ant)+1 % number of sensors or number of positions along the array
xn = (-(N-1)/2:+(N-1)/2)*dx_ant; % antenna positions along x
yn = zeros(N,1); % antenna positions along y
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TARGETS
Np = 5; % number of targets
z0 = 0; % height of the targets
% ground range coordinates of the targets
y_max = sqrt(r_max.^2 - (H-z0).^2);
y_min = sqrt(r_min.^2 - (H-z0).^2);
y0 = rand(1,Np)*(y_max-y_min) + y_min;
% azimuth coordinates of the targets
delta_x = delta_psi*r_min;
x0 = delta_x*(rand(1,Np)-0.5);
 
figure,
plot(yn,xn,'r<',y0,x0,'ko'), xlabel('y [m]'), ylabel('x [m]'), title('Simulated scene')
axis equal tight
legend('Sensors','Targets','Location','SouthWest')
x_min = delta_x/2 + 10;
ylim([-x_min x_min])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SIMULATING THE ACQUISITIONS AFTER MATCHED FILTERING
delay_min = r_min*2/c*0.8; % 0.8 = min delay
delay_max = r_max*2/c*1.2; % 1.2 = max delay
% fast time axis
dt = 1/2/B; % time sampling interval
t = (delay_min:dt:delay_max);
r = t*c/2;  Nr = length(r); % range axis
Drc = zeros(N,Nr);
for n = 1:N % cycle over all sensors
    dn = 0;
    for p = 1:Np % sum over all targets
        Rn = sqrt( (xn(n)-x0(p)).^2 + y0(p)^2 + (H-z0)^2);
        g = sinc((r-Rn)/rho_r)*exp(-1i*4*pi/lambda*Rn);
        dn = dn + g;
    end
    Drc(n,:) = dn;
end
figure,
imagesc(r,xn,abs(Drc)), axis xy , title('Range compressed data - abs')
colormap('jet')
xlabel('range [m]'), ylabel('sensor position [m]')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DATA PROCESSING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FOCUSING BY DFT
% spatial frequency axis
Nf = 2^ceil(log2(2*N)) % power of two (faster)
fx = (-Nf/2:Nf/2-1)/Nf/dx_ant;
df = fx(2)-fx(1); % spatial frequency sampling interval

rho_f = 1/Ax; % spatial frequency resolution
% Fourier transform
Dfr = fftshift(fft(Drc,Nf,1),1);
% 
figure,
imagesc(r,fx,abs(Dfr)), axis xy , title('Focusing by DFT')
xlabel('range [m]'), ylabel('spatial frequency [1/m]'), colormap('jet')
drawnow
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTERPOLATE IN XY
% coordinates of the pixels
z_ref = z0   % 
% x axis
rho_x = lambda/2/Ax*r_min; % min x resolution
dx = rho_x/2; % sampling in x (azimuth)
 
x_ax = (-x_min:dx:x_min); Nx = length(x_ax) % x axis
dy = rho_r/2; % sampling in y (ground range)
% y axis
y_max = sqrt(r(end).^2 - (H-z_ref).^2); 
y_ax = (0:dy:y_max); Ny = length(y_ax)
 
Y = ones(Nx,1)*y_ax;
X = x_ax(:)*ones(1,Ny);
 
R_xy = sqrt( X.^2 + Y.^2 + (H-z_ref)^2);
F_xy = 2/lambda*X./R_xy;
 
Imm_xy = interp2(r,fx,abs(Dfr),R_xy,F_xy);
figure,
imagesc(y_ax,x_ax,abs(Imm_xy)), axis xy , title('Focusing by DFT')
xlabel('y [m]'), ylabel('x [m]'), colormap('jet')
axis equal tight
hold on, plot(yn,xn,'r<',y0,x0,'wo')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TIME DOMAIN BACK PROJECTION
S = zeros(Nx,Ny,N);
dr = r(2)-r(1);
barra = waitbar(0,'Back projecting');
% interpolation and phase rotation of single antennas
for n = 1:N
    waitbar(n/N,barra)
    Rn_xy = sqrt( (X-xn(n)).^2 + (Y-yn(n)).^2 + (H-z_ref)^2);
    S(:,:,n) = interp1(r,Drc(n,:),Rn_xy).*exp(+1i*4*pi/lambda*Rn_xy);
end
close(barra)
 
% Backprojected images from individual antennas
figure,
for p = 1:4
    subplot(2,2,p)
    n = round(N/4*p);
    imagesc(y_ax,x_ax,abs(S(:,:,n))), axis xy equal tight,
    title(['Sensor number ' num2str(n)])
    xlabel('y [m]'), ylabel('x [m]')
    hold on, plot(yn(n),xn(n),'r<',y0,x0,'wo')
end
colormap('jet')
 
F = mean(S,3); % integral over all antennas
figure,
imagesc(y_ax,x_ax,abs(F)), axis xy , title('Focusing by TDBP')
xlabel('y [m]'), ylabel('x [m]'), colormap('jet')
axis equal tight
hold on, plot(yn,xn,'r<',y0,x0,'wo')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%