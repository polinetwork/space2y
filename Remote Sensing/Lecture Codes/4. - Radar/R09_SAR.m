clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ACQUISITION AND FOCUSING OF SAR DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% RADAR PARAMETERS
c = 3e8; % wave propagation velocity [m/s]
f0 = 1e10; % carrier frequency [Hz]
lambda = c/f0 % wavelength [m]
rho_r = 10 % range resolution [m]
B = c/2/rho_r; % bandwidth [Hz]
rho_x = 10 % along-track resolution (azimuth) [m]
Lx = 2*rho_x % physical antenna length (azimuth)
delta_psi = lambda/Lx; % illumination angle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FLIGHT GEOMETRY
H = 650e3; % orbit height [m]
v = 7000; % velocity w.r.t. Earth [m/s]
theta0 = 30/180*pi; % pointing in elevation [rad]
y_mid = H*tan(theta0); % ground range min and max
y_min = y_mid - 1000;
y_max = y_mid + 1000;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SYNTHETIC APERTURE
r_min = sqrt(y_min^2 + H^2); % slant range min
r_max = sqrt(y_max^2 + H^2); % slant range max
As = lambda/Lx*r_max % maximum synthetic aperture 
dx_ant = Lx/2*0.5 % spatial sampling of the synthetic aperture
x_amb = lambda/2/dx_ant*r_min % ambiguous interval if no antenna pattern is used
PRI = dx_ant/v % Pulse Repetition Interval
PRF = 1/PRI; % Pulse Repetition Frequency
Ltot = 4*As; % Total length of the simulated orbit
xa = (-Ltot/2:dx_ant:Ltot/2); % Radar position along the orbit
Nx = length(xa);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TARGETS
Np = 5; % number of targets
z0 = 0; % target height
%  y coordinate (random)
y0 = rand(1,Np)*(y_max-y_min) + y_min;
%  x coordinate (random)
delta_x = Ltot-As;
x0 = delta_x*(rand(1,Np)-0.5);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% RAW DATA AFTER MATCHED FILTERING
delay_min = r_min*2/c; % 
delay_max = r_max*2/c; % 
% time axis
dt = 1/2/B; % sampling interval
t = (delay_min:dt:delay_max);
r = t*c/2;
Nr = length(r); % distances
dr = r(2)-r(1);
Drc = zeros(Nx,Nr);
% target position expressed in range
r0 = sqrt(y0.^2 + (H-z0).^2);

for n = 1:Nx % loop on Radar position
    dn = 0;
    for p = 1:Np % loop on target position
        Rn = sqrt( (xa(n)-x0(p)).^2 + r0(p)^2);
        psi_n = (xa(n)-x0(p))./Rn;
        f = sinc(psi_n/delta_psi).^2; % antenna pattern
        %f = 1; % what happens without antenna pattern?
        g = f*sinc((r-Rn)/rho_r)*exp(-1i*4*pi/lambda*Rn);
        dn = dn + g;
    end
    Drc(n,:) = dn;
end
figure,
imagesc(r,xa,abs(Drc)), axis xy , title('range compressed data - abs')
xlabel('slant range [m]'), ylabel('sensor position [m]')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FOCUSING BY DFT  - JUST TO SHOW IT'S WRONG FOR A SAR!!!
% frequency axis
Nf = 2^ceil(log2(2*Nx)) % 
fx = (-Nf/2:Nf/2-1)/Nf/dx_ant;
df = fx(2)-fx(1);
% transform
Dfr = fftshift(fft(Drc,Nf,1),1);
% 
figure,
imagesc(r,fx,abs(Dfr)), axis xy , title('Focused data by DFT')
xlabel('slant range [m]'), ylabel('spatial frequency [1/m]')
drawnow, 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FOCUSING BY 1D MATCHED FILTERING
Dfr = zeros(Nx,Nr);
Ns = round(As/2/dx_ant);
x_filter = (-Ns:Ns)*dx_ant;
for k = 1:Nr
    R = sqrt(x_filter.^2 + r(k).^2);
    azimuthFilter = exp(1i*4*pi/lambda*R);
    Dfr(:,k) = conv2(Drc(:,k),azimuthFilter(:),'same');
end
% 
figure,
imagesc(r,xa,abs(Dfr)), axis xy , title('Focused data by 1D Matched filter')
xlabel('slant range [m]'), ylabel('azimuth [m]')
hold on, plot(r0,x0,'wo')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FOCUSING BY TIME DOMAIN BACK PROJECTION (TDBP)
[X,R] = ndgrid(xa,r);
wbar = waitbar(0,'Backprojecting');
S = 0;
for n = 1:Nx
    waitbar(n/Nx,wbar)
    Rn_xr = sqrt( (X-xa(n)).^2 + R.^2);
    % to speed-up the computation, only the samples within a synthetic 
    % aperture are computed
    ind_x = max(1,n-Ns):min(Nx,n+Ns);
    Rn_xr = Rn_xr(ind_x,:);
    Sn = zeros(Nx,Nr);
    Sn(ind_x,:) = interp1(r,Drc(n,:),Rn_xr).*exp(+1i*4*pi/lambda*Rn_xr);
    S = S + Sn;
end
close(wbar)

figure,
imagesc(r,xa,abs(S)), axis xy , title('Focused image by TDBP')
xlabel('slant range [m]'), ylabel('azimuth [m]')
hold on, plot(r0,x0,'wo')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ANALYSIS OF PULSE RESPONSE AFTER FOCUSING
figure
p = 1
[t,k] = min(abs(r-r0(p)))
for foc = 1:3
    switch foc
        case 1
            s = Dfr(:,k);
            Title = '1D Matched filter';
        case 2
            s = S(:,k);
            Title = 'TDBP';
        case 3
            s = sinc((xa-x0(p))/rho_x);
            Title = 'theoretical model';
        otherwise
    end
    subplot(3,1,foc), plot(xa,abs(s)/max(abs(s))), grid
    xlabel('azimuth [m]'), xlim([-5 5]*rho_x + x0(p))
    title(Title)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
