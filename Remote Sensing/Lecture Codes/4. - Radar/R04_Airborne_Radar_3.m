clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SIMULATION OF THREE POINT TARGETS AND EVALUATION OF SNR AFTER MATCHED
% FILTERING (PULSE COMPRESSION)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SYSTEM PARAMETERS
f0 = 1e9; % central frequency
c = 3e8; % speed of light
s0 = 10^-(1.5); % backscatter coefficient (soil & rocks)
H = 4000; % flight height 
dphi = 40/180*pi; % azimuth beamwidth
dteta = 40/180*pi; % elevation beamwidth
teta0 = 40/180*pi; % antenna pointing
B = 100e6; % transmitted bandwdith
% transmitted power
Pt = 1 % 1 W peak power (1 W = 30 dBm)
F = 4; % noise figure
T_ref = 290; % reference noise temperature
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DERIVED PARAMETERS
lambda = c/f0; % wavelength
pho_r = c/2/B; % range resolution
r_min = H/cos(teta0-dteta/2); % min range
r_max = H/cos(teta0+dteta/2); % max range

r = linspace(r_min,r_max,100); % range axis
theta = asin(sqrt(1 - (H./r).^2)); % incidence angle

L = lambda/(40/180*pi); % antenna length
A = L*L; % antenna area
G = 4*pi/(lambda^2)*A % antenna gain

PRI = 2*r_max*2/c; % a factor of 2 is assumed to leave some margin
duty_cycle = 0.8; % typical value for an FMCW system
Tobs = PRI*duty_cycle; % observation time

K = 1.38e-23; % Boltzmann constant
N0 = K*T_ref*F; % Power spectral density of noise
Pnoise = N0*B % noise power before filtering
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TRANSMITTED WAVEFORM (CHIRP)
alfa = B/Tobs % chirp rate
dt = 1/2/B; % sampling time (factor of 2 to leave some margin)
Nt = 4*round(Tobs/dt) + 1; % symmetric time axis (no problems related to half-samples)
t = [-(Nt-1)/2:(Nt-1)/2]*dt;
% transmitted signal
g = double(abs(t)<=Tobs/2).*exp(1i*pi*alfa*t.^2);
% Fourier transform
G = fftshift(fft(g)); 
% frequency axis
f_ax = (-(Nt-1)/2:(Nt-1)/2)/Nt/dt;

% compensation of the delay in Matlab's fft implementation
tau = -t(1);
G = G.*exp(1i*2*pi*f_ax*tau);

figure;
subplot(2,1,1), plot(f_ax/1e6,abs(G)), grid
xlabel('frequency [MHz]'), title('abs(G(f))')
subplot(2,1,2), plot(f_ax/1e6,angle(G)), grid
xlabel('frequency [MHz]'), title('angle(G(f))')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% WAVEFORM AFTER MATCHED FILTERING
h = conv2(g,conj(fliplr(g)),'same') * dt;

figure
subplot(2,1,1), plot(t,abs(g)), grid
xlabel('time [s]'), title('abs(g)')
subplot(2,1,2), plot(t,abs(h)), grid
xlabel('time [s]'), title('abs(h)')

figure
subplot(2,1,1), plot(t,real(h)), grid
xlabel('time [s]'), title('real(h)'), xlim([-5 5]/B)
subplot(2,1,2), plot(t,imag(h)), grid
xlabel('time [s]'), title('imag(h)'), xlim([-5 5]/B)

% Fourier Transform
H =  fftshift(fft(h));
H = H.*exp(1i*2*pi*f_ax*tau);

figure;
subplot(2,1,1), plot(f_ax/1e6,abs(H)), grid
xlabel('frequency [MHz]'), title('abs(H(f))')
subplot(2,1,2), plot(f_ax/1e6,angle(H)), grid
xlabel('frequency [MHz]'), title('angle(H(f))')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SIMULATION OF THREE POINT TARGETS
R = r_max/2 + pho_r*[-10 0 10]; % distances [m]
RCS = [300 150 450];   % m^2

% Radar equation 
G = 4*pi/(lambda^2)*A; 
Pr = Pt./( (4*pi*R.^2).^2 )*G*A.*RCS;

% received signal after demodulation
s_rx = 0;
for p = 1:length(R)
    tau = 2*R(p)/c;
    gp = double(abs(t-tau)<=Tobs/2).*exp(1i*pi*alfa*(t-tau).^2);
    s_rx = s_rx + sqrt(Pr(p))*gp*exp(1i*4*pi/lambda*R(p));
end

% matched filtering (range compression in Radar jargon)
s_rc = conv2(s_rx,conj(fliplr(g)),'same') * dt;

% Peak height
Eg = sum(abs(g).^2)*dt; % = Till
Peaks = sqrt(Pr)*Eg

figure
r = t*c/2; % time/range conversion
subplot(3,1,1), plot(t,abs(s_rx)), grid
xlabel('time [s]'), title('abs(received signal)')
subplot(3,1,2), plot(t,abs(s_rc)), grid
xlabel('time [s]'), title('abs(compressed signal)')
subplot(3,1,3), plot(r,abs(s_rc)), grid, xlim([min(R)-10 max(R)+10])
xlabel('range [m]'), title('abs(compressed signal) - close up')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EVALUATION OF THE SNR FOR THE THREE TARGETS

% noise before matched filtering
Pw = N0/dt; % white noise with spectral density = N0
w = sqrt(Pw/2)*(randn(1,Nt)+1i*randn(1,Nt));
% noise after filtering
v = conv2(w,conj(fliplr(g)),'same') * dt;
Pv_exp = var(v) % experimental measurement of noise power
%
Pv = N0*Eg   % theory

% signal before filtering
s_rx_noise = s_rx + w;
s_rc_noise = conv2(s_rx_noise,conj(fliplr(g)),'same') * dt;

figure
subplot(3,1,1), plot(t,abs(s_rx_noise)), grid
xlabel('time [s]'), title('abs(received signal)')
subplot(3,1,2), plot(t,abs(s_rc_noise)), grid
xlabel('time [s]'), title('abs(compressed signal)')
subplot(3,1,3), plot(r,abs(s_rc_noise)), grid, xlim([min(R)-10 max(R)+10])
xlabel('range [m]'), title('abs(compressed signal)')

% SNR
SNR_exp = 10*log10( (Peaks.^2)/Pv_exp ) % experimental measurement
SNR_th = 10*log10( Pr*Tobs/N0 ) % theoretical values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% MATCHED FILTERING IS CALLED RANGE-COMPRESSION BC THE INITIAL PULSE IS VERY
% LARGE WHILE THE FILTERED ONE IS VERY SHORT

% typical values:
% [...] 100 kW 
% power for an airborne radar: 1 kW
% human bodies 100 W
% 2W maximum output value for UMTS mobile phone
% 1W for GSM phone
% 500 mW [...]
% 200 mW power class 3



