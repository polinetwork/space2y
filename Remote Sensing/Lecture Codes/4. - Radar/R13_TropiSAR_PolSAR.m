%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLE OF APPLICATION OF POLARIMETRIC SAR PROCESSING OF A TROPICAL FOREST
%
% THIS SCRIPT MAKES USE OF A SUBSET OF THE SAR DATA COLLECTED BY ONERA
% DURING THE CAMPAIGN TROPISAR, FOUNDED BY THE EUROPEAN SPACE AGENCY IN THE
% THE CONTEXT OF PHASE-0 STUDY FOR THE P-BAND MISSION BIOMASS
% THE FINAL REPORT OF THE TROPISAR CAMPAIGN CAN BE DOWNLOADED AT
% https://earth.esa.int/c/document_library/get_file?folderId=21020&name=DLFE-899.pdf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load data
if 1
    clear all
    close all
    clc
    
    load('~/RSEOS_data/TropiSAR_Pol')
    I(:,:,2) = sqrt(2)*I(:,:,2);
end
whos
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Intensity images
Pol{1} = 'HH';
Pol{2} = 'HV';
Pol{3} = 'VV';
for ch = 1:3
    figure, imagesc(x_ax,r_ax,20*log10(abs(I(:,:,ch))))
    xlabel('azimuth [m]'), ylabel('range [m]')
    title(Pol{ch})
    colormap('gray')
end

% RGB image
figure, imagesc(x_ax,r_ax,20*log10(abs(I)))
xlabel('azimuth [m]'), ylabel('range [m]')
title('Red = HH, Green = HV, Blue = VV')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% De-speckling by local average
filter = hamming(11); 
filter = filter/sum(filter); % normalizazion (to get an average)
I2 = real(I(:,:,1).*conj(I(:,:,1))); 
I2f = conv2(conv2(I2,filter(:),'same'),filter(:)','same');
figure, imagesc(x_ax,r_ax,10*log10(I2f)), colorbar
colormap('gray'), cax = caxis;
figure, imagesc(x_ax,r_ax,10*log10(I2)), colorbar
colormap('gray'), caxis(cax);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Forming an image in the Pauli basis
Ip(:,:,1) = 1/sqrt(2)*(I(:,:,1) - I(:,:,3)); % Red = HH - VV
Ip(:,:,2) = 1/sqrt(2)*(I(:,:,2)); % Green = HV
Ip(:,:,3) = 1/sqrt(2)*(I(:,:,1) + I(:,:,3)); % Blue = HH + VV

% RGB image
figure, imagesc(x_ax,r_ax,20*log10(abs(Ip)))
xlabel('azimuth [m]'), ylabel('range [m]')
title('Red = HH-VV, Green = HV, Blue = HH+VV')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% With some filtering
I2f = zeros(size(I));
for ch = 1:3
    I2 = real(Ip(:,:,ch).*conj(Ip(:,:,ch)));
    I2f(:,:,ch) = conv2(conv2(I2,filter(:),'same'),filter(:)','same');
end
%I2f(:,:,2) = 4*I2f(:,:,2); this is to make the forest look green!
figure, imagesc(x_ax,r_ax,I2f)
xlabel('azimuth [m]'), ylabel('range [m]')
title('Red = HH-VV, Green = HV, Blue = HH+VV')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HH/VV phase
Ihhvv = I(:,:,1).*conj(I(:,:,3));
Ihhvv = conv2(conv2(Ihhvv,filter(:),'same'),filter(:)','same');
figure, imagesc(x_ax,r_ax,angle(Ihhvv))
xlabel('azimuth [m]'), ylabel('range [m]')
title('HH/VV phase'), colorbar
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
