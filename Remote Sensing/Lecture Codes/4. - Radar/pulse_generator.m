function [g,signal]  = pulse_generator(t,Tobs,B,tipo)

switch tipo
    case 1
        signal = 'sinc';
        g = sinc(t*B);
    case 2
        signal = 'constant';
        g = 1;
    case 3
        signal = 'up-chirp';
        alfa = B/Tobs;
        g = exp(+1i*pi*alfa*(t).^2);
    case 4
        signal = 'down-chirp';
        alfa = B/Tobs;
        g = exp(-1i*pi*alfa*(t).^2);
    case 5
        signal = 'up-chirp + dw-chirp';
        alfa = B/Tobs;
        g = exp(+1i*pi*alfa*(t).^2) + exp(-1i*pi*alfa*(t).^2);
    case 6
        signal = 'triangular chirp';
        alfa = 2*B/Tobs;
        gu = exp(+1i*(pi*alfa*(t).^2 + pi*B*t)).*(t<=0);
        gd = exp(-1i*(pi*alfa*(t).^2 - pi*B*t)).*(t>0);
        g = gu+gd;
    case 7
        signal = 'sinc train - Tp = 50/B';
        Tp = 50/B;
        k_max = ceil(Tobs/2/Tp);
        g = 0;
        for k = -k_max:k_max
            gk = sinc((t-k*Tp)*B);
            g = g + gk;
        end
    case 8
        signal = 'sinc train  - Tp = 3/B';
        Tp = 3/B;
        k_max = ceil(Tobs/2/Tp);
        g = 0;
        for k = -k_max:k_max
            gk = sinc((t-k*Tp)*B);
            g = g + gk;
        end
    case 9
        signal = 'noise';
        Nt = length(t);
        g = randn(1,Nt) + 1i*randn(1,Nt);
        f = sinc(t*B);
        g = conv2(g,f,'same');
    case 10 
        signal = 'up-chirp train ';
        Tchirp = Tobs/4;
        alfa = B/Tchirp;
        Tp = (Tobs-Tchirp)/2;
        g = 0;
        for k = -1:1
            gk = exp(+1i*pi*alfa*(t-k*Tp).^2).*rectpuls((t-k*Tp)/Tchirp);;
            g = g + gk;
        end
    otherwise
end
% time window to limit total signal duration
g = g.*rectpuls(t/Tobs);
