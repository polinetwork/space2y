%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ESTIMATION OF THE AMPLITUDE A OF A UNIFORM DISTRIBUTION U(0,A)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear, clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N_max = 100
Nr = 1e3;
A = 1
x = A*rand(N_max,Nr);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for N = 1:N_max
    A_2avg = 2*mean(x(1:N,:),1);
    A_max = max(x(1:N,:),[],1);
    
    E_2avg(N) = mean(A_2avg);
    E_max(N) = mean(A_max);
    E_max_th(N) = N/(N+1)*A;
    
    err = A_2avg - A;
    rmse_2avg(N) = sqrt(mean(err.^2));
    rmse_2avg_th(N) = 2*sqrt(A^2/12/N);
    
    err = A_max - A;
    rmse_max(N) = sqrt(mean(err.^2));
    rmse_max_th(N) = A*sqrt( N/(N+2) + 1 - 2*N/(N+1) );
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure
subplot(3,1,1), plot(1:N,E_2avg,1:N,E_max,1:N,E_max_th,'r--'), grid
xlabel('number of samples'), title('expected value')
legend('LS','ML','ML th')

std_2avg = sqrt(rmse_2avg.^2 - (E_2avg-A).^2);
std_max = sqrt(rmse_max.^2 - (E_max-A).^2);
subplot(3,1,2), plot(1:N,std_2avg,1:N,std_max), grid
xlabel('number of samples'), title('standard deviation')
legend('LS','ML')

subplot(3,1,3), plot(1:N,rmse_2avg,1:N,rmse_max,1:N,rmse_max_th,'r--'), grid
xlabel('number of samples'), title('rmse')
legend('LS','ML','ML th')
