%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATION OF Nr INDEPENDENT REALIZATIONS OF A FILTERED PROCESS AND
% ESTIMATION OF EXPECTED VALUE AND AUTOCORRELATION 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear, clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GNERATION OF STOCHASTIC WHITE PROCESSES
dt = 1e-3; % sampling time
t = (0:5000)*dt; % time axis
Nt = length(t);
Tobs = Nt*dt % total observation time
Nr = 100; % number of realizations
x = randn(Nr,Nt); % white process
h = fir1(50,.1); % filter (row vector <=> works on time dimensions)
x = conv2(x,h,'same');

figure
for n = 1:4 % displays the first 4 realizations
    subplot(4,1,n), plot(t,x(n,:)), grid
    xlabel('time [s]'), title(['x_' num2str(n) '(t)'])
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ESTIMATION BY AVERAGING ACROSS DIFFERENT REALIZATIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Estimation of exepected value and power ( = variance, since E[x] = 0)
Ex = mean(x,1);
Px = mean(x.^2,1);
figure,
subplot(2,1,1), plot(t,Ex), grid
xlabel('time [s]'), title('E[x]')
subplot(2,1,2), plot(t,Px), grid
xlabel('time [s]'), title('Px')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Estimation of autocorrelazion by averaging across realizations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n_max = 100;
delta_t = (-n_max:n_max)*dt; % time axis for autocorrelation
Rx = zeros(2*n_max+1,Nt);
for n = -n_max:n_max
    xxx = x.*circshift(x,[0 n]); % circshift = circular shift
    Rx(n+n_max+1,:) = mean(xxx,1);
end
figure
imagesc(t,delta_t,Rx), colorbar, axis xy
xlabel('time [s]'), ylabel('time lag [s]')
title('R_x(time lag,t)')
Rx_avg_real = Rx;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ESTIMATION BY TEMPORAL AVERAGING, UNDER THE ASSUMPTION OF STATIONARIETY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Estimation of exepected value and power ( = variance, since E[x] = 0)
Ex = mean(x,2);
Px = mean(x.^2,2);
figure,
subplot(2,1,1), plot(1:Nr,Ex), grid
xlabel('realizations'), title('E[x]')
subplot(2,1,2), plot(1:Nr,Px), grid
xlabel('realizations'), title('Px')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Estimation of the Power Spectral Density
% FT
Nf = 2^ceil(log2(Nt))
%Nf = 2*Nf
[X,f] = dft(transpose(x),t,Nf);
% transpose is necessary since dft works along columns

X = transpose(X); % realizations, frequency
X = X*dt; % scale factor for dimensional consistentcy 
% Power Spectral Density
Sx = 1/Tobs*abs(X).^2;
figure
for n = 1:4 % displays four realizations
    subplot(4,1,n), plot(f,Sx(n,:)), grid
    xlabel('frequency [Hz]'), title(['S_{x_' num2str(n) '}(f)'])
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Autocorrelation by inverse FT
Rx = idft(transpose(Sx),f,delta_t); 
% transpose is necessary since idft works along columns
Rx = transpose(Rx); % realizations, delta-time
% check: Rx should be real since x is a real-valued process
max_err_imag = max(abs(imag(Rx(:))))
Rx = real(Rx); 

% scale factor for dimensional consistentcy 
% (!!!: idft is based on built-in function ifft, that divides the result by
% the number of frequency points
df = f(2)-f(1);
Rx = Rx*df*Nf; 
%
figure
imagesc(delta_t,1:Nr,Rx), colorbar, axis xy
xlabel('time lag  [s]'), ylabel('realizations')
title('R_x(realizations,time lag)')
Rx_avg_temp = Rx;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Comparison between the two approaches
Rx1 = mean(Rx_avg_real,2); % average over time (stationariety)
Rx2 = mean(Rx_avg_temp,1); % average over realizations 
figure
plot(delta_t,Rx2,delta_t,Rx1), grid
xlabel('time lag [s]'), 
title('R_x(time lag)')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%







