%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ML ESTIMATION OF THE FREQUENCY OF A COMPLEX SINUSOID 
% AND EVALUATION OF THE ESTIMATION ERROR
% ML ESTIMATOR = Fourier Transform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DATA GENERATION
% true values of frequency and phase
f0 = .1; 
phi0 = 2*pi*(rand-.5);
% time axis
N = 5 
dt = 1;
t = (0:N-1)'*dt;
% phase model
phi = 2*pi*f0*t + phi0;
% clean data
y_clean = exp(1i*phi);

% noise
s2w = .1;
Ne = 1e4;
w = sqrt(s2w/2)*(randn(N,Ne) + 1i*randn(N,Ne));

% noisy data
y = y_clean*ones(1,Ne) + w;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ACCURACY ANALYSIS ASSUMING LINEARITY 
% assumption: phases are small => exp(1i*phi) = 1 + 1i*phi
% => imag(y) = phi + imag(w)

% Uncorrelated noise <=> LS is BLUE
A = [2*pi*t ones(N,1)];
Ai = inv(A'*A)*A';
% Phase noise covariance
std_phase_noise = sqrt(s2w/2)
var_phase_noise = std_phase_noise^2;
Cw_phi = var_phase_noise*eye(N);
% Performance evaluation
Ce = Ai*Cw_phi*Ai';
% frequency estimation accuracy (std)
std_f0_th = sqrt(Ce(1,1))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NON-LINEAR ESTIMATION VIA FT
% frequency sampling: df << theoretical accuracy, or you'll measure
% the quantization error!
df = std_f0_th/10;   
% frequency axis
f_ax = (-.5:df:.5);

% FT
W = exp(-1i*2*pi*f_ax(:)*t');
Y = W*y;
figure, imagesc(1:Ne,f_ax,abs(Y)), axis xy
xlabel('experiments'), ylabel('frequency')
% maximization
[t,k] = max(abs(Y),[],1);
% estimated frequency
f0_stim = f_ax(k);
% estimation error
err = f0_stim - f0;
std_f0_ML = sqrt(mean(err.^2))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

