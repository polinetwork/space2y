clear, clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATION OF UNIFORM-, NORMAL-, AND RAYLEIGH-DISTRIBUTED RANDOM VARIABLES
% 
% x = rand(N1,N2); generates N1xN2 independent random variables,
% uniformely distributed between 0 and 1
%
% x = randn(N1,N2); generates N1xN2 independent random variables,
% normally distributed with expected value 0 and variance 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ANALYTICAL EXPRESSION OF THE DISTRIBUTION
% Choice of the distribution
distribution = 2
switch distribution
    case 1 %  uniform distribution between a and b
        a = -1;
        b = 2;
        dx = .1 % amplitude of the bin
        x0 = (-3:dx:3); % positions of the bins 
        % probability density function
        fx_th = 1/(b-a)*rectpuls((x0-(a+b)/2)/(b-a));
        % Expected value
        Ex_th = (b+a)/2
        % Variance
        Vx_th = 1/12*(b-a).^2
    case 2 % Normal distribution N(u,s2)
        u = 1; % expected value
        s2 = 2; % variance
        dx = .1 % amplitude of the bin
        x0 = (-20:dx:20); % positions of the bins 
        % probability density function
        fx_th = 1/sqrt(2*pi*s2)*exp(-1/2/s2*(x0-u).^2);
        % Expected value
        Ex_th = u
        % Variance
        Vx_th = s2
    case 3 % Rayleigh 
        s2 = 2;
        dx = .1 % amplitude of the bin
        x0 = (-1:dx:20); % positions of the bins 
        % probability density function
        fx_th = x0/s2.*exp(-1/2/s2*x0.^2);
        fx_th(x0<0) = 0;
        % Expected value
        Ex_th = sqrt(pi/2*s2)
        % Variance
        Vx_th = (4-pi)/2*s2
    otherwise
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N = 1e4 % number of experiments
N_repetition = 50 % number of repetiotion of N experiments
% RANDOM EXPERIMENTS
switch distribution
    case 1 % uniform
        x = a + (b-a)*rand(N,N_repetition);
    case 2 % normal
        x = u + sqrt(s2)*randn(N,N_repetition);
    case 3 % Rayleigh
        a = sqrt(s2)*randn(N,N_repetition);
        b = sqrt(s2)*randn(N,N_repetition);
        x = sqrt(a.^2 + b.^2);   
        clear a b
    otherwise
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure
plot(1:N,x(:,1)), grid
xlabel('experiments'), title('x')
