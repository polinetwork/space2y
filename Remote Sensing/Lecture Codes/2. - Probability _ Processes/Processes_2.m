%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREDICTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATION AND ESTIMATION OF AUTOCORRELATION
if not(exist('x'))
    clear, clc
    Processes_1 % 
    Rx = Rx2; % estimated autocorrelation
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correlation coefficient (assuming stationariety and zero-mean)
n0 = find(delta_t==0)
Rx_0 = Rx(n0); % Autocorrelation in 0
rho_x = Rx/Rx_0; % Correlation coefficient
figure, plot(delta_t,rho_x), grid
xlabel('time lag [s]') , title('\rho_x')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prediction of x(t+tau) based on x(t) (1-step predictor)
k = 4;
tau = k*dt %
Rx_tau = Rx(n0+k);% Autocorrelation at tau
pho_tau = Rx_tau/Rx_0 % Correlation coefficient at tau
% Prediction
xs = pho_tau*x(:,1:end-k); % 
err = xs - x(:,k+1:end); % Prediction error
% Evaluation of the Mean Square Error
MSE = mean((err(:)).^2) 
% Theoretical evaluation
Px = mean(abs(x(:)).^2);
MSE_th = Px*(1-(pho_tau).^2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%