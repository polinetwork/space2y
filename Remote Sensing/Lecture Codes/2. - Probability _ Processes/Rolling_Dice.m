%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ROLLING REGULAR AND TRICKED DICE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
% elementary events
elem_events = [1,2,3,4,5,6];
% number of experiments
N = 1e4 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
exercise = 4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RANDOM EXPERIMENT GENERATION
switch exercise
    case 1
        % Ex 1: one regular die
        d = ceil(6*rand(N,1)); % elementary events = {1,2,3,4,5,6}
        % NOTE 1:
        % x = rand(N1,N2); generates N1xN2 independent random variables,
        % uniformely distributed between 0 and 1
        % NOTE 2:
        % ceil = round up
    case 2
        % Ex 2: one tricked die
        % Probability distribution = [1/10 1/10 1/10 1/10 1/10 1/2]
        %
        x = ceil(10*rand(N,1)); % elem events = {1,2,3,4,5,6,7,8,9,10}
        d = x;
        d(d>6) = 6; % elem events = {1,2,3,4,5,6}
    case 3
        % Ex 3: Two independent dice
        x = ceil(10*rand(N,2));
        d1 = x(:,1); d1(d1>6) = 6;
        d2 = x(:,2); d2(d2>6) = 6;
    case 4
        % Ex 4: Two non-independent dice
        % P(d2|d1<6) = [1/10 1/10 1/10 1/10 1/10 1/2]
        % P(d2|d1=6) = [0 0 0 0 0 1]
        x = ceil(10*rand(N,2));
        % first die
        d1 = x(:,1); d1(d1>6) = 6;
        % second die, if first die < 6
        d2_caso_1 = x(:,2); d2_caso_1(d2_caso_1>6) = 6;
        % second die, if first die = 6
        d2_caso_2 = 6*ones(N,1);
        % second die
        d2 = zeros(size(d1));
        k1 = find(d1<6); d2(k1) = d2_caso_1(k1);
        k2 = find(d1==6); d2(k2) = d2_caso_2(k2);
    otherwise
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ANALYSIS OF PROBABILITY DISTRUBUTION
if exercise < 3
    % Estimation of probability
    P = zeros(1,6);
    for v = 1:6
        P(v) = sum(d==elem_events(v))/N;
    end
    sum(P) % check - must yield 1
    figure,
    subplot(2,1,1), stem(elem_events,P), grid, ylim([0 1])
    title('P(d)')
    xlabel('outcomes of the random experiment')
else
    % Estimation of the joint probability of two dice
    P_d1_d2 = zeros(6,6);
    for v1 = 1:6
        for v2 = 1:6
            t = and(d1==elem_events(v1),d2==elem_events(v2));
            P_d1_d2(v1,v2) = sum(t)/N;
        end
    end
    sum(sum(P_d1_d2,1),2) % check - must yield 1
    fig = figure;
    subplot(2,2,1)
    imagesc(elem_events,elem_events,P_d1_d2), colorbar
    title('P(d1,d2)')
    xlabel('second die')
    ylabel('first die')
    % Probability of any single die
    P_d1 = sum(P_d1_d2,2);
    P_d2 = sum(P_d1_d2,1);
    figure,
    subplot(2,1,1), stem(elem_events,P_d1), grid, ylim([0 1])
    title('P(first die)')
    subplot(2,1,2), stem(elem_events,P_d2), grid, ylim([0 1])
    title('P(second die)')
    
    % Conditional probability
    P_d1_cond_d2 = P_d1_d2./(ones(6,1)*P_d2);
    P_d2_cond_d1 = P_d1_d2./(P_d1*ones(1,6));
    figure(fig)
    subplot(2,2,3)
    imagesc(elem_events,elem_events,P_d1_cond_d2), colorbar
    title('P(d1|d2)')
    xlabel('second die')
    ylabel('fist die')
    subplot(2,2,4)
    imagesc(elem_events,elem_events,P_d2_cond_d1), colorbar
    title('P(d2|d1)')
    xlabel('second die')
    ylabel('fist die')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

