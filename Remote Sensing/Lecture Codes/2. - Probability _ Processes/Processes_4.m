%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ESTIMATION OF DELAY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0 % load an audio track
    clear, clc
    [x,Fs] = audioread('01 Track 1.mp3');
    x = x(:,1); % considers just a single track
    t_min = 5;
    Fs % Sampling Frequency [Hz]
    % cut the time axis
    t_max = t_min + 20
    ind = round(t_min*Fs):round(t_max*Fs);
    x = x(ind,:);
    dt = 1/Fs;
    Nt = length(ind);
    Tobs = Nt*dt
    t = (0:Nt-1)*dt + t_min;
    return
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Delay
tau = 1;
k = round(tau/dt)
y = [zeros(k,1);x]; % pad the signal with k zeroes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 1% Sum the original and the delayed signal
    y = [x;zeros(k,1)] + y;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Estimation by cross-correlation
% FT
Nf = 2^ceil(log2(Nt))
[X,~] = dft(x,t,Nf);
[Y,f] = dft(y,t,Nf);
% cross-spectrum
S_yx = 1/Tobs*Y.*conj(X); 
% time axis
delta_t = (-5:dt:5);
R_yx = idft(S_yx,f,delta_t); 
% check: Rx should be real since x is a real-valued process
max_err_imag = max(abs(imag(R_yx(:))))
R_yx = real(R_yx); 
figure
subplot(2,1,1), plot(delta_t,R_yx), grid
xlabel('time lag [s]'), title('R_y_x')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

