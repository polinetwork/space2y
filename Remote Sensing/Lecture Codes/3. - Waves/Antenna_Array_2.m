%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TRANSMISSION USING A LINEAR ANTENNA ARRAY
% We will compute the 2D field irradiated by such an array, and hence
% identify the prevailing radiating direction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear, clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ARRAY PARAMETERS
c = 3e8; % propagation velocity  [m/s]
f0 = 1.2e9; % carrier frequency [Hz]
lambda = c/f0 % wavelength [m]

La = 2 % Array aperture [m]
N = 15 % Number of elements
delta_z = La/N; % spacing between neighbouring elements
zn = (-(N-1)/2:(N-1)/2)*delta_z; % vertical position of each element
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DIRECTION OF RADIATION
% Direction
theta = -30
% Corresponding spatial frequency
fz = sind(theta)/lambda;
% Coefficients
an = exp(-1i*2*pi*zn(:)*fz);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIELD CALCULATION
dy = 1; % spatial sampling along y [m]
dz = dy; % spatial sampling along z [m]
z = (-200:dz:200); % vertical axis [m]
y = (1:dy:400); % horizontal axis [m]
[Z,Y] = ndgrid(z,y); % Matrices of (z,y) coordinates

% N.B:
% [X,Y] = meshgrid(1:3,4:6)   % i.e. [col-wise-rep, row-wise-rep]
%  >>X =
%       1     2     3
%       1     2     3
%       1     2     3
%  >>Y =
%       4     4     4
%       5     5     5
%       6     6     6
%    
% [X,Y] = ndgrid(1:3,4:6)    % % i.e. [row-wise-rep, col-wise-rep]
%  >>X =
%       1     1     1
%       2     2     2
%       3     3     3
%  >>Y =
%       4     5     6
%       4     5     6
%       4     5     6

% field calculation
E = 0;
for n = 1:N
    n
    Rn = sqrt(Y.^2 + (Z-zn(n)).^2); % distances from the n-th antenna
    En = exp(-1i*2*pi/lambda*Rn)./Rn; % spherical wave from the n-th antenna
    E = E + an(n)*En; % total field
end
E_db = 20*log10(abs(E));
figure, imagesc(y,z,E_db), axis xy
colorbar, xlabel('y [m]'), ylabel('z [m]')
title('Radiated field intensity [dB]')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



