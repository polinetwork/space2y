%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% RECEPTION USING A LINEAR ANTENNA ARRAY
% The goal is ot detect the direction of a received signal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all, close all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Incident direction
theta_i = 30; % [deg]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ARRAY PARAMETERS
c = 3e8; % propagation velocity  [m/s]
f0 = 1.2e9; % carrier frequency [Hz]
lambda = c/f0 % wavelength [m]

La = 2 % Array aperture [m]
N = 15 % Number of elements
delta_z = La/N; % spacing between neighbouring elements
zn = (-(N-1)/2:(N-1)/2)*delta_z; % vertical position of each element
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DIRECTION SCANNING
% Directions tested by setting the array coefficients
theta = linspace(-90,90,501);% i.e. we are willing to probe all these
% different directions (-90, +90) [deg] and check wheter we receive a
% signal along those directions

% Corresponding spatial frequencies
fz = sind(theta)/lambda;
% Coefficients
an = exp(-1i*2*pi*zn(:)*fz);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% RECEPTION OF A SINGLE FREQUENCY IN THE FREQUENCY DOMAIN
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generation of the received signal
sn = exp(-1i*2*pi/lambda*sind(theta_i)*zn(:));

% Combined signal: Fourier Transform of the signal received at each antenna
s = an'*sn;
figure
subplot(2,1,1), plot(theta,abs(s)), grid
xlabel('theta [deg]'), 
title('Array response at f = f_0')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% RECEPTION OF A SHORT PULSE IN THE TIME DOMAIN
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pulse parameters
B = 10e6; % Bandwidth [Hz]
T_obs = 10/B; % Total observation time [s]
dt = 1/4/B; % sampling time
t = (-T_obs/2:dt:T_obs/2); % time axis

% Delays with respect to the antenna in z = 0
tau = 1/c*sind(theta_i)*zn(:);

% Signals received at the N antennas
sn = zeros(N,length(t));
for n = 1:N
    sn(n,:) = sinc((t-tau(n))*B)*exp(-1i*2*pi*f0*tau(n));
end
% NOTE THAT: f0*tau(n) = 1/lambda*sind(teta_i)*zn(:);
% => SAME PHASE AS IN THE FREQUENCY DOMAIN APPROACH

figure
subplot(2,1,1), imagesc(t,zn,abs(sn)), axis xy
xlabel('time [s]'), ylabel('antennas position [m]')
title('Received signals (abs)')
subplot(2,1,2), imagesc(t,zn,angle(sn)), axis xy
xlabel('time [s]'), ylabel('antennas position [m]')
title('Received signals (phase)')

% Combined signal (for each time instant)
S = an'*sn;
figure
imagesc(t,theta,abs(S)), axis xy
xlabel('time [s]'), ylabel('theta [deg]'), 
title('Array response')
% RMK.: As it can be observed, the peak will be in correspondence of the
% actual direction of the received signal

% Note that if we set e.g. N = 5 (rather than N=15), aliasing will occur!
% Which can be seen in the replicas overlapping in this last plot.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

