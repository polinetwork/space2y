%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RF (like) SIGNALS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%if 0 % loads two audio signals
    clear, clc
    [x,Fs1] = audioread('01 Lonely Woman.m4a');
    [y,Fs2] = audioread('Tous les memes.m4a');
    Fs1
    Fs2 % check the sampling frequency is the same!
    fs = Fs1 % Sampling Frequency [Hz]
    % time axis (same for both signals)
    Nt = min([size(x,1) size(y,1)]);
    Nt = 1e6 % consider shorter signals - just to speed up the computation 
    x = x(1:Nt,:);
    y = y(1:Nt,:);
    dt = 1/fs;
    t = (0:Nt-1)*dt;
%end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spectral Analysis
Nf = 2*2^ceil(log2(Nt));
[X,f] = dft(x,t,Nf);
Y = dft(y,t,Nf);
figure, 
subplot(2,1,1), plot(f,abs(X)), grid, xlabel('frequency [Hz]')
subplot(2,1,2), plot(f,abs(Y)), grid, xlabel('frequency [Hz]')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Creates an RF signal which embeds the two signals previously loaded
f0 = 1e+4; % carrier frequency
theta0 = 0; % carrier phase

% rf signal
s = x.*(cos(2*pi*f0*t(:)+theta0)*ones(1,2)) - y.*(sin(2*pi*f0*t(:)+theta0)*ones(1,2));
% N.B. *ones(1,2) is just to replicate twice
% Spectral Analysis
S = dft(s,t,Nf);
figure, 
subplot(3,1,1), plot(f,abs(S)), grid, xlabel('frequency [Hz]')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXTRACTION of the in-phase (I) and quadrature (Q) components
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Digital extraction by one complex multiplication and filtering
f1 = f0 
theta1 = theta0
% Try other values:
% • theta1 = theta0 + pi/2 -> When we retrieve the two separate signals,
%                             they become flipped (i.e. Re{} and Im{} are
%                             flipped)
% • theta1 = theta0 + pi/4 -> The two signals get mixed! If we try playing
%                             the recovered signals they are both mixes of
%                             the original ones

s_demod = s.*(exp(-1i*(2*pi*f1*t(:)+theta1))*ones(1,2));
% Spectral Analysis
S_demod = dft(s_demod,t,Nf);
subplot(3,1,2), plot(f,abs(S_demod)), grid, xlabel('frequency [Hz]')
% Filter design
B = 15e3;
Bn = B/fs

Nh = 1000
h = fir1(Nh,Bn);
% Spectral Analysis
th = (-Nh/2:Nh/2)*dt;
H = dft(h,th,Nf);
hold on, plot(f,abs(H)*max(abs(S_demod(:)))),
% Filtering
z_rx = conv2(s_demod,h(:),'same');
% Spectral Analysis
Z_rx = dft(z_rx,t,Nf);
subplot(3,1,3), plot(f,abs(Z_rx)), grid, xlabel('frequency [Hz]')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Listen to the results
% soundsc(s,fs)
% soundsc(real(z_rx),fs)
% soundsc(imag(z_rx),fs)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Comparison against the original complex signal
z = x + 1i*y;
% energy
Ez = sum(abs(z.^2),1)*dt
Ez_rx = sum(abs(z_rx.^2),1)*dt
% phase (Interferogram)
Interf = z.*conj(z_rx);
Interf_filt = conv2(Interf,ones(101,1),'same'); % This filter is basically a Moving Average!
figure, plot(t,angle(Interf_filt)), grid
title('phase error')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%