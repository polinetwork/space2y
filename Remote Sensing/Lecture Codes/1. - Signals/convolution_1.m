clear, clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generation of two random signals
Nx = 6;
Nh = 5;
x = randn(1,Nx); 
h = randn(1,Nh); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Convolution as a double sum
Ny = Nx + Nh - 1 % length of the output signal y
y = zeros(1,Ny);
% number of samples used for the computation of each sample of y
num_samp = zeros(1,Ny); 
for n = 0:Ny-1
    y(n+1) = 0;
    num_samp(n+1) = 0;
    for k = 0:Nx-1
        n_minus_k = n-k;
        if and(n_minus_k>=0,n_minus_k<=Nh-1)
            y(n+1) = y(n+1) + x(k+1)*h(n_minus_k+1);
            num_samp(n+1) = num_samp(n+1) + 1;
        end
    end
end
y
num_samp
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Convolution using Matlab built-in function conv or conv2
y = conv2(x,h)

% Option 'valid' issues only time instants computed using all samples
y = conv2(x,h,'valid')

% Option 'same' issues the same time axis as the input signal
y = conv2(x,h,'same')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%