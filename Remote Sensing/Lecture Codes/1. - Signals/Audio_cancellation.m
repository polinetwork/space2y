%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CANCELLATION OF A SIGNAL FROM THE COMBINATION OF TWO MICROPHONES
% We extract the sound coming from one of the sources, from the reading of
% two close microphones
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%if 0
    clear, clc
    [s1,Fs1] = audioread('01 Lonely Woman.m4a');
    [s2,Fs2] = audioread('Tous les memes.m4a');
    Fs1
    Fs2
    Fs = Fs1 % Sampling Frequency [Hz]
    % time axis
    Nt = min([size(s1,1) size(s2,1)]);
    s1 = s1(1:Nt,:);
    s2 = s2(1:Nt,:);
    dt = 1/Fs;
    t = (0:Nt-1)*dt;
    return
%end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% time interval
t_min = 18
t_max = t_min+10
ind = round(t_min*Fs):round(t_max*Fs);
Nt = length(ind);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% delays
dtau = [0 0; 0 .9*dt];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Signals at two microphones
d = zeros(Nt,2,2); % time,traces(left/right), microphone
tt = (-1000:1000)'*dt;
for n = 1:2

    n
    h = sinc((tt-dtau(1,n))/dt);
    d1 = conv2(s1(ind,:),h,'same');
    h = sinc((tt-dtau(2,n))/dt);
    d2 = conv2(s2(ind,:),h,'same');
    d(:,:,n) = d1 + d2;
end
clear d1 d2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% COHERENT CANCELATION OF s1
% d1(t) = s1(t) + s2(t)
% d2(t) = s1(t) + s2(t-tau)
% => ss(t) = d1(t)-d2(t) = s2(t)-s(t-tau)
ss = d(:,:,1)-d(:,:,2); 

% Observing the signals (type clear sound to stop)
% soundsc(s1(ind,:),Fs) % source 1
% soundsc(s2(ind,:),Fs) % souce 2
% soundsc(d(:,:,1),Fs) % signal at microphone 1
% soundsc(d(:,:,2),Fs) % signal at microphone 2
% soundsc(ss,Fs) % difference between the two microphones
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECTRAL ANALYSIS
% => ss(t) = d1(t)-d2(t) = s2(t)-s(t-tau)
% => SS(f) = S2(f)*(1-exp(-1i*2*pi*f*tm)) = S2(f)*H(f)
Nf = 2^10
f = (-Nf/2:Nf/2-1)/Nf/dt;
H = zeros(Nf,1);
% d1-d2
H = exp(-1i*2*pi*f*dtau(2,1))-exp(-1i*2*pi*f*dtau(2,2)); 
figure, plot(f,20*log10(abs(H))), grid, ylim([-40 10])

if 0 % 
    [S2,f_ss] = dft(s2(ind,:),ind*dt,2^ceil(log2(length(ind))));
    S2 = S2/max(abs(S2(:)));
    subplot(2,1,1), plot(f_ss,abs(S2)), grid
    xlabel('frequency [Hz]')
    
    [SS,f_ss] = dft(ss,ind*dt,2^ceil(log2(length(ind))));
    SS = SS/max(abs(SS(:)));
    subplot(2,1,2), plot(f_ss,abs(SS),f,abs(H)), grid
    xlabel('frequency [Hz]')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Restoring low frequencies via digital filtering
ind_freq_pos = (Nf/2+1:Nf);
f_norm = f(ind_freq_pos)*dt;
H_inv = 1./(abs(H(ind_freq_pos))+.1);
filter = firls(1000,2*f_norm,H_inv);
% firls() -> Matlab runs an optimization problem where it seeks for a smooth
% frequency response, using Least Squares, given the samples in H_inv
% see help firls in Matlab documentation

F = fftshift(fft(filter,Nf));
plot(f,20*log10(abs(F.*H))), grid, ylim([-40 10])

ss_filt = conv2(ss,filter(:),'same');
%soundsc(ss_filt,Fs)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


