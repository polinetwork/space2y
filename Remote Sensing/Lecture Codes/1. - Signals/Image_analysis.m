%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IMAGE ANALYSIS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear, clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% loads an RGB image
if 0
    z = imread('Barros_500GP.jpg');
else
    z = imread('Photo0075.jpg');
end
whos
figure, imagesc(z)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% conversion to double - required to manipulate data, (e.g. performing FT,
% convolutions, etc. which cannot be done w/ unsigned integers)
I = double(z);
% scaling - Class double RGB images need to be scaled between 0 and 1
I = I/max(I(:)); % 0-to-1 normalization
figure, imagesc(I)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Imaging single channels (RGB)
for ch = 1:3
    Isc = zeros(size(I));
    Isc(:,:,ch) = I(:,:,ch);
    figure, imagesc(Isc)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FILTERING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Low pass filter
% Filtered image will get more blurry
filter_lp_rows = hamming(35)';
filter_lp_columns = hamming(35);
If = zeros(size(z));
for ch = 1:3
    t = conv2(I(:,:,ch),filter_lp_rows,'same');
    t = conv2(t,filter_lp_columns,'same');
    If(:,:,ch) = t;
end

% scaling
If = If/max(If(:));
figure, imagesc(I), title('Original image')
figure, imagesc(If), title('Filtered image')

%% Contours (high pass filter)
% It detects edges (meant as high contrast regions)
filter_hp_rows = [1 -1]; % numerical derivative
filter_hp_columns = [1 -1]'; 
Ic = zeros(size(I));
for ch = 1:3
    tx = conv2(I(:,:,ch),filter_hp_rows,'same');
    ty = conv2(I(:,:,ch),filter_hp_columns,'same');
    % Ic(:,:,ch) = ty;
    Ic(:,:,ch) = sqrt(tx.^2 + ty.^2);  
end
% scaling
Ic = Ic/max(Ic(:));
figure, imagesc(I), title('Original image')
figure, imagesc(Ic), title('Filtered image')

% High contrast binary image
Ic_contr = Ic>.1;
figure, imagesc(Ic_contr), title('Filtered image')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SPECTRAL ANALYSIS
[Ny,Nx] = size(Ic(:,:,1));
Nfy = 2^ceil(log2(Ny));
Nfx = 2^ceil(log2(Nx));
fy = (-Nfy/2:Nfy/2)/Nfy; % normalized frequency along columns
fx = (-Nfx/2:Nfx/2)/Nfx; % normalized frequency along rows
IF = zeros(Nfy,Nfx,3);
for ch = 1:3
    t = fft(I(:,:,ch),Nfy,1); % Fourier transform along columns
    t = fft(t,Nfx,2); % Fourier transform along rows
    IF(:,:,ch) = fftshift(t); %
end
% scaling
IF = IF/max(abs(IF(:)));
figure, imagesc(fx,fy,abs(IF)), title('Fourier Transform')
% in dB
figure, imagesc(fx,fy,20*log10(abs(IF(:,:,1)))), 
title('Fourier Transform - dB')

IF = zeros(Nfy,Nfx,3);
for ch = 1:3
    % 2D Fourier Transform: we simply have to apply FT along a direction
    % and then along the other
    t = fft(If(:,:,ch),Nfy,1); % Fourier transform along COLUMNS (<->1)
    t = fft(t,Nfx,2);          % Fourier transform along ROWS    (<->2)
    IF(:,:,ch) = fftshift(t); %
end
% in dB
figure, imagesc(fx,fy,20*log10(abs(IF(:,:,1)))), 
title('Fourier Transform - dB')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Spectral analysis of the filter
h = conv2(filter_lp_columns(:),filter_lp_rows);
H = fftshift(fft2(h,Nfy,Nfx)); % fft2 does the two squential ffts automatically...
figure, imagesc(h), title('2D filter')
figure, imagesc(fx,fy,20*log10(abs(H))), 
title('Fourier Transform of the 2D filter- dB')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DOWN-SAMPLING AND INTERPOLATION
x = (0:Nx-1);
y = (0:Ny-1);
dws = 2;
I_dws = I(1:dws:end,1:dws:end,:);
x_dws = x(1:dws:end);
y_dws = y(1:dws:end);
figure, imagesc(x,y,I)
figure, imagesc(x_dws,y_dws,I_dws)
% Interpolation
method = 'nearest'
method = 'linear'
Inn = zeros(Ny,Nx,3);
for ch = 1:3
    t = interp1(y_dws,I_dws(:,:,ch),y,method);
    Inn(:,:,ch) = interp1(x_dws,t',x,method)';
end
figure, imagesc(x,y,Inn), title(method)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Coordinate trasformation
r = (0:Ny);
teta = linspace(-pi,pi,1001);
[Teta,R] = ndgrid(teta,r);
X = R.*cos(Teta);
Y = R.*sin(Teta);
Ip = zeros(length(teta),length(r),3);
for ch = 1:3
Ip(:,:,ch) = interp2(x-mean(x),y-mean(y),I(:,:,ch),X,Y);
end
figure, imagesc(r,teta,Ip)
xlabel('r'), ylabel('teta')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


