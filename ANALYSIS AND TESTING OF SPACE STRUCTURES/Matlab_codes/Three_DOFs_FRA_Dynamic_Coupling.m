clear all
close all
clc
format long

fr_start = 52;  % Hz
fr_end = 80;    % Hz
fr_step = +2;   % Hz
color = 0.2;
figure

for fr_SC = fr_start:fr_step:fr_end
    color = color + 0.7/((fr_end-fr_start)/fr_step);
    %% DATA for a SC longitudinal frequency requirement > 60 Hz
    % LV = Launch Vehicle
    % SC = SpaceCraft
    fr_LV = 52 %(45+60)/2;     % Hz
    %fr_SC = 61;     % Hz
    m_LV = 30000;   % Kg
    m_SC = 1000;    % Kg

    %% Stiffness and mass parameters of the system
    k1 = m_LV * (2 * pi * fr_LV)^2; %[N/m]
    k2 = m_SC * (2 * pi * fr_SC)^2; %[N/m]
    m1 = m_LV;
    m2 = m_SC;
    
    c1_cr = (2*(k1*m1)^0.5); %[N/(m/s^2)]
    c2_cr = (2*(k2*m2)^0.5); %[N/(m/s^2)]
    
    c1 = 0.05 * c1_cr; %[N/(m/s^2)]
    c2 = 0.05 * c2_cr; %[N/(m/s^2)]

    % Large Mass
    m0 = 1e6;   %[kg]
    k0 = 0.0001; %[N/m]
    %% MATRICES
    I = eye(3);
    ZEROS = zeros(3);

    M = diag([m1, m2, m0]);                         % Mass Matrix
    K = [k1+k2 -k2 -k1; -k2 k2 0; -k1 0 +k1+k0];    % Stiffness Matrix
    % Damping Matrix: choose among multiple possibilities: 
    %       1 = Structural;
    %       2 = Rayleigh (Viscous, Proportional); 
    %       3 = Lumped Parameters (Viscous).
    Damping = 3; % <-- User's choice
    if      Damping == 1
        G = 0.04;   % Structural Damping coefficient
    elseif Damping == 2
        C=3.1338E-01*M + 7.9379E-05*K;
    elseif Damping == 3
        C = [c1+c2 -c2 -c1; -c2 c2 0; -c1 0 +c1];        
    end

    %% FRF (classical method)

    % Info about natural frequencies
    [V,D]=eig(K,M);
    freq = sort(diag(D).^0.5/(2*pi))

    c = 0;
    P = zeros(length(K),1);
    P(3) = 1;
    for fr = 5.:0.1:110
        c = c + 1;
        freq(c) = fr;
        omega = 2 * pi * fr;
        if  Damping == 1
            Response(c,:) = (-omega^2*M+K+1i*G*K) \ P;
        elseif Damping == 2 || Damping == 3
            Response(c,:) = (-omega^2*M+1i*omega*C+K) \ P;
        end
        FRF_ampl(c,:) = omega^2 * abs(Response(c,:))* m0; 
    end
    freq = freq';
    
    % Plotting the Transfer Function of node 2
    figure(1)
    FRF_0 = FRF_ampl(:,3);
    FRF_2 = FRF_ampl(:,2)./FRF_0; %u2_pp
    plot(freq,FRF_2,'k','DisplayName',strcat('fr\_SC = ',num2str(fr_SC),' Hz'),'Color',[1-color,1-color,1-color])
    hold on
    legend
    grid on
    xlim([30 90])
    xlabel('\it Hz','interpreter','latex');
    ylabel('$\frac{\ddot{x}_{2}}{\ddot{x}_{0}}\hspace{3 mm}$','interpreter','latex','rotation',0,'FontSize',18);
    figure(2)
    FRF_1 = FRF_ampl(:,1)./FRF_0; %u1_pp
    plot(freq,FRF_1,'k','DisplayName',strcat('fr\_SC = ',num2str(fr_SC),' Hz'),'Color',[1-color,1-color,1-color])
    hold on
    legend
    grid on
    xlim([30 90])
    xlabel('\it Hz','interpreter','latex');
    ylabel('$\frac{\ddot{x}_{1}}{\ddot{x}_{0}}\hspace{3 mm}$','interpreter','latex','rotation',0,'FontSize',18);
end
