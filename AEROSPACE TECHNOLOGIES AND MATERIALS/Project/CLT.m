%% >> CLT_MAKE : DIRECT
% Creates the elastic matrix of a laminate
% applying the Classic Lamination Theory in the DIRECT form
clear all; close all; clc; %#ok<CLALL>
%% Data of the laminate
% Data of the lamination sequence:
% layup = array of the angles (�) of the lamination sequence
layup = input('Enter the array of the angles (�) of the lamination sequence: \n(Default: [+45,-45,-45,+45])\n(Note: assume that the actuator will be embedded in the first half of the sequence)\n');
if isempty(layup)
    layup = [+45,-45,-45,+45];
end
% mates = array of the materials (ID from MATERIALS.dat file) of the lamination sequence
mates = input('Enter the array of the materials (ID from MATERIALS.dat file) of the lamination sequence: \n(Default: [4,4,4,4] = ALL Carbon FB)\n');
if isempty(mates)
    mates = [4,4,4,4];
end
    % Example 1a : Symmetric Omogeneous Thick laminate 
%     layup = [+45,-45,90,90,90 , 90,90,90,-45,+45];  
%     mates = 1*ones(size(layup)); % ALL glass UD
    % Example 1b : Symmetric mixed-materials Thick laminate 
%     layup = [+45,-45,90,90,90 , 90,90,90,-45,+45]; 
%     mates = [2,2,3,3,3,3,3,3,2,2]; % symmetric material distribution
    % Example 2 : Symmetric Omogeneous Thin laminate
%     layup = [+45,-45,-45,+45];
%     mates = 4*ones(size(layup)); % ALL Carbon FB
    % Example 3 : UNsymmetric mixed-materials Thin laminate
%     layup = [0,+45,+45];
%     mates = [1,2,2]; % Materials sequence
%% Geometry:
l = 300; % mm
b = 60; % mm
%% Actuator distribution
ACT = input('Select actuator: 1=SMA or 2=PZT\n(Default: 1)\n');
if isempty(ACT)
    ACT = 1;
end
if ACT == 1
    step = 10; % (mm) Distance between SMA wires = "influence width" for the SMA
elseif ACT == 2
    step = 25; % (mm) Width of the piezo
end
Fact = input('Enter the force (N) of the single actuator:\n(Default = 6 N)\n');
if isempty(Fact)
    Fact = 6;
end
% Number of plies (from the bottom) external to the embedding plane
pp = input('Enter the number of plies external to the embedding plane of the actuator:\n(Default = 1)\n(Note: remember that the embedding plane is in the forst half of the sequence)\n');
if isempty(pp)
    pp = 1;
end 
%% Thermal load
PHASE = input('Select the phase you are considering: 1=Curing; 2=Activation\n(Default = 1)\n');
if isempty(PHASE)
    PHASE = 1;
end
if PHASE == 1
    % Curing:
    dT=(20-130); % Temperature decrease after curing
    Fact = 0 ; % (N) Force of the SINGLE SMA wire/ of the single PZT; when zero only thermal effects are included
elseif PHASE == 2
    % Actuation:
    if ACT == 1 % SMA
        dT=0 ; % During activation, the panel re-warms to (about) the curing level, therefore "canceling" the previous thermal effects
    elseif ACT == 2 % PZT 
        dT=(20-130); % Temperature stresses are added to activation
    end
end
    
%% Mechanical properties of the materials in the file 'MATERIALS.dat':
%   Name of the material
%   Ex   (MPa) system N-mm-MPa
%   Ey   (MPa)
%   Gxy  (MPa)
%   vyx  (-) Poisson
%   tply (mm) Thickness
fileID = fopen('MATERIALS.dat');
MAT=dlmread('MATERIALS.dat','',1,0);
names = textscan(fileID,'%s %s %s %s',1);
fclose(fileID);
%
N = length(layup); % Number of the plies
H = sum(MAT(5,mates)); % (mm) Total thickness of the laminate
%% Notes on notation:
% pedix i indicates the ply
% xy = coordinates of the ply
% XY = coordinates of the laminate
% -------------------------------------
% IF the material was the same for all plies (only orientation was changed),
% then Qi stiffness matrix would be the same for all.
% HAVING DIFFERENT MATERIALS, a loop is used to create the different matrices
% And save them through a structure.
% --------------------------------------------
%% Empty arrays/struct allocation 
% Height
%   As a convention, I use zero on the BOTTOM surface of the laminated and
%   Z> 0 upwards, then the 1st sheet is the one UNDER all
Z=zeros(N+1,1);
% Heights relative to the geometric mean surface (zero to the mid-surface)
Z0=zeros(N+1,1);
Z0(1)=Z(1)-H/2;
% Stiffness matrix of the ply in ply-axes
Qi_xy_all(1:N)={zeros(3,3)};
% Stiffness matrix of the ply in laminated axes
Qi_XY_all(1:N)={zeros(3,3)};
% Rotation matrix from ply-axes to laminated axes
T_all(1:N)={zeros(3,3)};
% Sollicitation
stress_T_XY_all(1:N)={zeros(3,1)};
%% Calculations ply-by-ply
for i=1:N
    % Mechanical properties of the ply:
    Ex   =  MAT(1,mates(i)); % (MPa) system N-mm-MPa
    Ey   =  MAT(2,mates(i)); % (MPa)
    Gxy  =  MAT(3,mates(i)); % (MPa)
    vxy  =  MAT(4,mates(i)); % (-) Poisson
    tply =  MAT(5,mates(i)); % (mm) Thickness
    a1   =  MAT(6,mates(i)); % (1/K) CTE in fiber direction
    a2   =  MAT(7,mates(i)); % (1/K) CTE in transverse direction
    % i-th Flexibility Matrix in ply-axes (xy lowercase)
    Fi_xy = [   1/Ex        -vxy/Ex 0       ; ...
                -vxy/Ex     1/Ey    0       ; ...
                0           0       1/Gxy   ];
    %% 1) Creating stiffness matrix of the ply in ply-axes
    Qi_xy=inv(Fi_xy);
    Qi_xy_all{i}=Qi_xy;
    % Height
    Z(i+1)=Z(i)+tply;
    % Heights relative to the geometric mean surface (zero to the mid-surface)
    Z0(i+1)=Z(i+1)-H/2;
    %% 2) Rotation of matrices in laminated axes
    % To avoid creating a thousand arrays, I use a structure.
    % Remember rotating the Stresses:
    % (Stess_laminate-axis) = inv(T)*(Stress_ply-axis)
    alpha=layup(i); % (�)
    c=cosd(alpha);
    s=sind(alpha);
    T=[ c^2     s^2     2*c*s   ;...
        s^2     c^2     -2*c*s  ;...
        -s*c    s*c     (c^2-s^2)];
    T_all{i}=T;
    %% +) Matrix R
    % Here I can USE or DO NOT use the transform matrix [R]
    % In the first case I will have the slips gamma_xy
    % In the second, deformation with mixed indexes eps_xy = gamma_xy/2
    % In the example, there is. Otherwise, just put: R = eye (3,3);
    % -----------------------------------------------------------------------------------
    R=diag([1 1 2],0);
    % R=eye(3,3);
    %% 3) Stiffness matrix of the ply in laminated axes
    Qi_XY = inv(T) *Qi_xy *R*T*inv(R); 
    Qi_XY_all{i}=Qi_XY;
    %% 4) Force fluxes in each ply due to thermal strains
    a_xy = [a1 ; a2 ; 0]; % CTE in ply axes
    a_XY = R*T*inv(R)*a_xy; % CTE in laminated axes
    stress_T_XY_all{i} = Qi_XY*a_XY*dT;
end
%% Integral sum
% Initialize matrices:
A = zeros(3,3);
B = A;
D = A;
%
N_T = zeros(3,1);
M_T = N_T;
%% Matrixes sum
for i=1:N % plies
% Note: direct CLT intergration refers to geometrical mid-plane coordinates!
    Qi_XY = Qi_XY_all{i};
    A = A + Qi_XY*( Z0(i+1)-Z0(i) );
    B = B + (1/2)*Qi_XY*( (Z0(i+1))^2-(Z0(i))^2 );
    D = D + (1/3)*Qi_XY*( (Z0(i+1))^3-(Z0(i))^3 );
    %
    stress_T_XY = stress_T_XY_all{i};
    N_T = N_T + stress_T_XY*( Z0(i+1)-Z0(i) );
    M_T = M_T + (1/2)*stress_T_XY*( (Z0(i+1))^2-(Z0(i))^2 );
end
%% Assembly
Q = [ A B; B D ]; % Stiffness of the laminated in laminated-axes
                  % Note: stiffness does not change including thermal
                  % effects, direct-CLT remains the same
F_T = [N_T ; M_T];
%% PRINT OF RESULTS 
fprintf('\n******** DIRECT CLT ********\n');
format shortG
display(Q);
%% Equivalent matrices:
% Astar = A/(H);
% display(Astar/1000); % GPa
% Bstar = 2*B/(H^2);
% display(Bstar/1000); % GPa
% Dstar = 12*D/(H^3); 
% display(Bstar/1000); % GPa 
%% Laminate in Command Window
fprintf('Lamination sequence:\n');
        fprintf('Z0(%d)= %+8.3f ---------------\n',N+1,Z0(N+1));
    for i=N:-1:1
        fprintf('\tPly %d, %3.1f�, %s \n',i,layup(i),names{mates(i)}{1});
        fprintf('Z0(%d)= %+8.3f ---------------\n',i,Z0(i));
    end
% ************************************************************************
%% >> CLT_SOLVE : INVERSE
% Given the stiffness matrix of the laminate and the solicitation state,
% derives stresses in each lamina
% by applying the classic theory of lamination in INVERSE form
fprintf('\n******** INVERSE CLT ********\n');
%% Solicitation state
fprintf('( F_act = %g N )\n',Fact);
% MECHANICAL
Nx = -Fact/step ;   % (N/mm) Negative, since it is compression
                    % Note : (Fact*Nsma)/(step*Nsma) 
                    % Total force/total width is equal to single-wire force/step
Ny = 0 ; % (N/mm)
Nxy= 0 ; % (N/mm) 
%
h_emb=Z0(pp+1); % (mm) embedding coordinate = distance from geometrical mid-plane
h = h_emb; % Approximate mid-plane with neutral-plane (true for thin laminates)
Mx = Nx*h; % (N) Compressive force below neutral axis, gives positive moment
My = 0 ; % (N)
Mxy= 0 ; % (N)
%
F_M = [Nx Ny Nxy Mx My Mxy]';
% + THERMAL
F = F_M + F_T;
%% 1) Generalized Deformations
% EPS = [eps0x eps0y gamma0xy kx ky kxy]'
EPS=Q\F; % inv(Q)*F;
display(EPS);
% 2) Single-lamination deformation state in laminated coordinates
% I initialize the three vectors as structures
% EPSi = [eps_x eps_y gamma_xy];
EPSi_XY_all(1:N) = {zeros(3,1)};
for i=1:N
    % Coord. of the mid-plane of the PLY
    z=( Z0(i+1)+Z0(i) )/2;
    EPSi_XY = EPS(1:3) + z*EPS(4:6);
    EPSi_XY_all{i} = EPSi_XY;
end
%% 3) Rotation of the deformations of the single ply in laminate coordinates
% R=eye(3,3);
% eps_xy = R*epstilde_xy = R*T*epstilde_XY = RT*R^(-1)*eps_XY
EPSi_xy_all(1:N) = {zeros(3,1)};
for i=1:N
    T=T_all{i};
    EPSi_xy = R*T*inv(R)* EPSi_XY_all{i}; %#ok<*MINV>
    EPSi_xy_all{i} = EPSi_xy;
end
%% 2) Stress state of the single ply in ply-coordinates
% SIGi = [sig_x sig_y tau_xy];
SIGi_xy_all(1:N) = {zeros(3,1)};
for i=1:N
    SIGi_xy_all{i}= Qi_xy*EPSi_xy_all{i};
end
%% PRINT OF RESULTS
fprintf('For the plies including embedding plane:\n');
for i=[pp+1,pp]
    fprintf('\tPly %d, Z0= %+6.3f:\n',i,Z0(i));
    fprintf('\t\te11 = %g\n',EPSi_XY_all{i}(1));
end
fprintf('Embedding plane: Z0 = h = %+6.3f mm\n',h) % =Z0(pp+1)
fprintf('\tCurvature k11 = %g 1/mm\n',EPS(4));
% Referred to ply mid-plane, then mean value linearized
% EPSi_XY_pp = ( EPSi_XY_all{pp}(1)+EPSi_XY_all{pp+1}(1))/2;
EPSi_XY_pp = EPSi_XY_all{pp}(1) + ...
    ( EPSi_XY_all{pp+1}(1) - EPSi_XY_all{pp}(1) )/( Z(pp+1)-Z(pp) )/2*(h+H/2);
fprintf('\tStrain e11 = %g\n',EPSi_XY_pp);
fprintf('\tContraction (with length l=%+6.0f mm): %+6.3f mm\n',l,l*EPSi_XY_pp)
%
C=inv(Q);
fprintf('\ta11 = %g mm/N\n',C(1,1));
fprintf('\tb11 = %g 1/N\n',C(1,4));
fprintf('\td11 = %g 1/N/mm\n',C(4,4));
Klam=-step/l/(C(1,1)+2*h*C(1,4)+(h^2)*C(4,4));
fprintf('\tKlam = %g N/mm\n',Klam);
%
fprintf('*********\nOnly for distributed uniform compression (SMA):\n');
alpha = l*EPS(4);
u_center = sign(EPS(4))* (1-cos(alpha/2))/EPS(4);
fprintf('\tApproximated central deflection when simply supported (with length l=%+6.0f mm): %+6.3f mm\n',l,u_center)
% u_tip=sign(EPSi_XY_pp)*(1-cos(l*EPS(4)))/EPS(4); % (mm) approximative deflection

